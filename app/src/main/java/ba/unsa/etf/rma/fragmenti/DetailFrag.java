package ba.unsa.etf.rma.fragmenti;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import java.util.ArrayList;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.aktivnosti.DodajKvizAkt;
import ba.unsa.etf.rma.aktivnosti.IgrajKvizAkt;
import ba.unsa.etf.rma.aktivnosti.KvizoviAkt;
import ba.unsa.etf.rma.klase.Kviz;
import ba.unsa.etf.rma.klase.KvizAdapter;
import ba.unsa.etf.rma.klase.KvizAdapterZaGrid;

public class DetailFrag extends Fragment {

    private GridView gridKvizovi;
    private ArrayList<Kviz> kvizovi = new ArrayList<>();

    private KvizAdapterZaGrid adapterKviz ;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_novi, container, false);
        //KOLIKO GOD DA SAM POKUSAVALA DA FRAGMENT XML NAZOVEM FRAGMENT_DETAIIL, IZ NEKOG RAZLOGA NE MOGU TO DA URADIM
        //NAZVALA SAM GA OVAKO I NE ZNAM DA LI CE PRAVITI PROBLEM PRILIKOM TESTIRANJA ALI JA IZ
        // NEKOG RAZLOGA NE MOGU DA GA NAZOVEM TAKO NA KOJI GOD NACIM POKUSAVALA DA KREIRAM XML

        gridKvizovi = (GridView) v.findViewById(R.id.gridKvizovi);


        ArrayList<Kviz> k = getArguments().getParcelableArrayList("kvizovi");
        kvizovi.addAll(k);

        adapterKviz = new KvizAdapterZaGrid(getActivity(), kvizovi);
        gridKvizovi.setAdapter(adapterKviz);

        gridKvizovi.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (KvizoviAkt.kviz.size() == 0) {
                    if (position == KvizoviAkt.kvizovi.size() - 1) {

                    } else {
                        Intent myIntent = new Intent(getActivity(), IgrajKvizAkt.class);
                        myIntent.putExtra("naziv", KvizoviAkt.kvizovi.get(position).getNaziv());
                        myIntent.putExtra("listapitanja", KvizoviAkt.kvizovi.get(position).getPitanja());
                        myIntent.putExtra("kategorija", KvizoviAkt.kvizovi.get(position).getKategorija());
                        getActivity().startActivityForResult(myIntent, 2);
                    }
                    //startActivity(myIntent);
                } else {
                    if (position == KvizoviAkt.kviz.size() - 1) {

                    } else {

                        Intent myIntent = new Intent(getActivity(), IgrajKvizAkt.class);
                        myIntent.putExtra("naziv", KvizoviAkt.kviz.get(position).getNaziv());
                        myIntent.putExtra("listapitanja", KvizoviAkt.kviz.get(position).getPitanja());
                        myIntent.putExtra("kategorija", KvizoviAkt.kviz.get(position).getKategorija());
                        getActivity().startActivityForResult(myIntent, 2);
                    }
                    //startActivity(myIntent);
                }
            }
        });

        gridKvizovi.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener()
    {
        @Override
        public boolean onItemLongClick (AdapterView < ? > parent, View view,int position, long id){
        if (KvizoviAkt.kviz.size() == 0) {
            if (position == KvizoviAkt.kvizovi.size() - 1) {
                Intent myIntent = new Intent(getActivity(), DodajKvizAkt.class);
                getActivity().startActivityForResult(myIntent, 0);
            } else {
                Intent myIntent = new Intent(getActivity(), DodajKvizAkt.class);
                myIntent.putExtra("naziv", KvizoviAkt.kvizovi.get(position).getNaziv());
                myIntent.putExtra("listapitanja", KvizoviAkt.kvizovi.get(position).getPitanja());
                myIntent.putExtra("kategorija", KvizoviAkt.kvizovi.get(position).getKategorija());
                KvizoviAkt.pozicija = position;
                //startActivity(myIntent);
                getActivity().startActivityForResult(myIntent, 1);
            }
        } else {
            if (position == KvizoviAkt.kviz.size() - 1) {
                Intent myIntent = new Intent(getActivity(), DodajKvizAkt.class);
                getActivity().startActivityForResult(myIntent, 0);
            } else {
                Intent myIntent = new Intent(getActivity(), DodajKvizAkt.class);
                myIntent.putExtra("naziv", KvizoviAkt.kviz.get(position).getNaziv());
                myIntent.putExtra("listapitanja", KvizoviAkt.kviz.get(position).getPitanja());
                myIntent.putExtra("kategorija", KvizoviAkt.kviz.get(position).getKategorija());
                KvizoviAkt.pozicija = position;
                getActivity().startActivityForResult(myIntent, 1);
                // startActivity(myIntent);
            }
        }
        return true;

            }
        });


        return v;
    }
}
