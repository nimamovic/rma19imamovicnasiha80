package ba.unsa.etf.rma.klase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Pair;

import java.util.ArrayList;

public class KvizoviDBOpenHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "mojaBaza.db";
    public static final int DATABASE_VERSION = 14;

    public static final String DATABASE_TABLE​_KVIZ = "Kvizovi";
    public static final String KVIZ_ID = "kviz_id";
    public static final String KVIZ_NAZIV = "naziv";
    public static final String KATEGORIJA_KVIZ = "idKategorija";
    public static final String KVIZ_PITANJA = "pitanja";

    private static final String DATABASE_CREATE​_KVIZ = "create table " + DATABASE_TABLE​_KVIZ + " (" + KVIZ_ID + " integer primary key autoincrement, " + KVIZ_NAZIV + " text not null, " + KATEGORIJA_KVIZ + " text not null," + KVIZ_PITANJA +" text not null);";

    public static final String DATABASE_TABLE​_KATEGORIJE = "Kategorije";
    public static final String KATEGORIJA_ID = "kategorija_id";
    public static final String KATEGORIJA_NAZIV = "nazivKategorije";
    public static final String KATEGORIJA_IKONA_ID = "idIkoneKategorije";

    private static final String DATABASE_CREATE​_KATEGORIJA = "create table " + DATABASE_TABLE​_KATEGORIJE+ " (" + KATEGORIJA_ID + " integer primary key autoincrement, " +  KATEGORIJA_NAZIV + " text not null, " + KATEGORIJA_IKONA_ID + " text not null);";

    public static final String DATABASE_TABLE​_PITANJA = "Pitanja";
    public static final String PITANJE_ID = "kategorija_id";
    public static final String PITANJE_NAZIV = "nazivKategorije";
    public static final String INDEX_TACNOG = "indexTacnog";
    public static final String PITANJE_ODGOVORI = "odgovori";

    private static final String DATABASE_CREATE​_PITANJA = "create table " + DATABASE_TABLE​_PITANJA + " (" + PITANJE_ID + " integer primary key autoincrement, " + PITANJE_NAZIV + " text not null, " +  INDEX_TACNOG + " text not null, " + PITANJE_ODGOVORI + " text not null);";

    public static final String DATABASE_TABLE​_RANGLISTA= "Ranglista";
    public static final String RANGLISTA_ID = "ranglista_id";
    public static final String RG_KVIZ = "nazivKviza";
    public static final String IGRAC = "igrac";
    public static final String POZCIJA_IGRACA = "pozcijaIgraca";
    public static final String PROCENAT = "procenat";

    private static final String DATABASE_CREATE​_RANGLISTU = "create table " + DATABASE_TABLE​_RANGLISTA + " (" + RANGLISTA_ID + " integer primary key autoincrement, " + RG_KVIZ + " text not null, " +  IGRAC + " text not null, " + POZCIJA_IGRACA + " text not null, " + PROCENAT + " text not null);";



    public KvizoviDBOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }



    public KvizoviDBOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DATABASE_CREATE​_KVIZ);
        db.execSQL(DATABASE_CREATE​_KATEGORIJA);
        db.execSQL(DATABASE_CREATE​_PITANJA);
        db.execSQL(DATABASE_CREATE​_RANGLISTU);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE​_KVIZ);
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE​_KATEGORIJE );
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE​_PITANJA);
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE​_RANGLISTA);
        onCreate(db);
    }

    public void brisi(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM "+ DATABASE_TABLE​_KATEGORIJE);
        db.execSQL("DELETE FROM "+ DATABASE_TABLE​_PITANJA);
        db.execSQL("DELETE FROM "+ DATABASE_TABLE​_RANGLISTA);
        db.execSQL("DELETE FROM "+ DATABASE_TABLE​_KVIZ);
    }

    public void upisiKategorijuUBazu(Kategorija k) {
        ContentValues novi = new ContentValues();
        novi.put(KATEGORIJA_NAZIV, k.getNaziv());
        novi.put(KATEGORIJA_IKONA_ID, k.getId());

        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(KvizoviDBOpenHelper.DATABASE_TABLE​_KATEGORIJE,null,novi);
    }

    public void upisiPitanjeUBazu(Pitanje p){
        ContentValues novi = new ContentValues();
        novi.put(PITANJE_NAZIV, p.getNaziv());
        String it = "";
        for(int i = 0; i < p.getOdgovori().size(); i++){
            if(p.getTacan() == p.getOdgovori().get(i)) {
                it = String.valueOf(i);
                break;
            }
        }
        novi.put(INDEX_TACNOG, it);
        String odg = "";
        for(int i = 0 ; i < p.getOdgovori().size(); i++){
            odg += p.getOdgovori().get(i);
            if(i != p.getOdgovori().size() -1) odg += ",";//mozda samo ","???????
        }
        novi.put(PITANJE_ODGOVORI, odg);
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(DATABASE_TABLE​_PITANJA,null,novi);
    }

    public ArrayList<Pair<String, String>> dohvatiKategorijeIzBazeZaKviz(){

        Cursor c;
        ArrayList<Pair<String, String>> lista = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();
        c = db.rawQuery("SELECT * FROM " + DATABASE_TABLE​_KATEGORIJE, null);

        int kat_id = c.getColumnIndexOrThrow(KATEGORIJA_ID);
        int katNaziv  = c.getColumnIndexOrThrow(KATEGORIJA_NAZIV);

        while (c.moveToNext()) {
            lista.add(new Pair<String, String>(c.getString(kat_id),c.getString(katNaziv)));
            System.out.println("udjeeeeeeeeeeee");
        }
        c.close();
        return lista;

    }

    public ArrayList<Pair<String, String>> dohvatiPitanjaIzBazeZaKviz(){

        Cursor c;
        ArrayList<Pair<String, String>> lista = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();
        c = db.rawQuery("SELECT * FROM " + DATABASE_TABLE​_PITANJA, null);

        int pit_id= c.getColumnIndexOrThrow(PITANJE_ID);
        int pitNaziv = c.getColumnIndexOrThrow(PITANJE_NAZIV);

        while (c.moveToNext()) {
            lista.add(new Pair<String, String>(c.getString(pit_id),c.getString(pitNaziv)));
         //   System.out.println("udjeeeeeeeeeeee");
        }
        c.close();
        return lista;

    }


    public void upisiKvizUBazu(Kviz k){
        ContentValues novi = new ContentValues();
        novi.put(KVIZ_NAZIV, k.getNaziv());
        ArrayList<Pair<String, String>> lista = dohvatiKategorijeIzBazeZaKviz();
        String idKat = "";
        for(int i = 0 ; i < lista.size(); i++){
            if(k.getKategorija().getNaziv().equals(lista.get(i).second)){
                idKat = lista.get(i).first;
            }
        }
        novi.put(KATEGORIJA_KVIZ, idKat);
        String idPit = "";
        lista = dohvatiPitanjaIzBazeZaKviz();
        for(int j = 0 ; j < k.getPitanja().size(); j++) {
            for(int i = 0 ; i < lista.size(); i++){
                if (k.getPitanja().get(j).getNaziv().equals(lista.get(i).second)) {
                    idPit += lista.get(i).first;
                    if(j != k.getPitanja().size() - 1) idPit+=",";
                }
            }
        }
        novi.put(KVIZ_PITANJA, idPit);
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(KvizoviDBOpenHelper.DATABASE_TABLE​_KVIZ,null,novi);
    }

    public void upisiRangUBazu(String nazivKviza, String ime, String procenat, String pozicija){
        ContentValues novi = new ContentValues();
        novi.put(RG_KVIZ, nazivKviza);
        novi.put(IGRAC, ime);
        novi.put(POZCIJA_IGRACA, pozicija);
        novi.put(PROCENAT, procenat);

        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(KvizoviDBOpenHelper.DATABASE_TABLE​_RANGLISTA,null,novi);
    }

    public ArrayList<Kategorija> citajKategorijeIzBaze(){
        ArrayList<Kategorija> listaKategorijaIzBaze = new ArrayList<>();
        Cursor c;
        SQLiteDatabase db = this.getReadableDatabase();
        c = db.rawQuery("SELECT * FROM " + DATABASE_TABLE​_KATEGORIJE, null);

        //int kat_id = c.getColumnIndexOrThrow(KATEGORIJA_ID);
        int katNaziv  = c.getColumnIndexOrThrow(KATEGORIJA_NAZIV);
        int katIdIkone = c.getColumnIndexOrThrow(KATEGORIJA_IKONA_ID);

        while (c.moveToNext()) {
            listaKategorijaIzBaze.add(new Kategorija(c.getString(katNaziv),c.getString(katIdIkone)));
            System.out.println("udjeeeeeeeeeeee");

        }
        c.close();
        return listaKategorijaIzBaze;
    }

    public ArrayList<Pitanje> citajPitanjaIzBaze(){
        ArrayList<Pitanje> listaPitanjaIzBaze = new ArrayList<>();
        Cursor c;
        SQLiteDatabase db = this.getReadableDatabase();
        c = db.rawQuery("SELECT * FROM " + DATABASE_TABLE​_PITANJA, null);

        //int pit_id= c.getColumnIndexOrThrow(PITANJE_ID);
        int pitNaziv = c.getColumnIndexOrThrow(PITANJE_NAZIV);
        int pitIndTacnog = c.getColumnIndexOrThrow(INDEX_TACNOG);
        int pitOdgovori = c.getColumnIndexOrThrow(PITANJE_ODGOVORI);

        while (c.moveToNext()) {
            String s = c.getString(pitOdgovori);
            String pom = "";
            int j = 0;
            ArrayList<String> odgovori = new ArrayList<>();
            for(int i = 0; i < s.length(); i++){
                if(s.charAt(i) == ',' || i == s.length() - 1){
                    if(i == s.length() - 1) i++;
                    pom = s.substring(j, i);
                    j = i;
                    j++;
                    odgovori.add(pom);
                }
            }
            String it = c.getString(pitIndTacnog);
            listaPitanjaIzBaze.add(new Pitanje(c.getString(pitNaziv), c.getString(pitNaziv), odgovori, odgovori.get(Integer.parseInt(it))));
        }
        c.close();
        return listaPitanjaIzBaze;
    }

    public ArrayList<Kviz> citajKvizoveIzBaze(){
        ArrayList<Kviz> listaKvizovaIzBaze = new ArrayList<>();
        Cursor c;
        SQLiteDatabase db = this.getReadableDatabase();
        c = db.rawQuery("SELECT * FROM " + DATABASE_TABLE​_KVIZ, null);

        int kvizNaziv = c.getColumnIndexOrThrow(KVIZ_NAZIV);
        int kvizKat = c.getColumnIndexOrThrow(KATEGORIJA_KVIZ);
        int kvizPit = c.getColumnIndexOrThrow(KVIZ_PITANJA);

        while(c.moveToNext()){
            String k = c.getString(kvizKat);
            String nazivKat = "";
            ArrayList<Pair<String,String>> listaKategroija = dohvatiKategorijeIzBazeZaKviz();
            for(int i = 0; i < listaKategroija.size(); i++){
                if(listaKategroija.get(i).first.equals(k)){
                    nazivKat = listaKategroija.get(i).second;
                    break;
                }
            }
            Kategorija katKviza = new Kategorija();
            ArrayList<Kategorija> kategorijeIzBaze = citajKategorijeIzBaze();
            for(int i = 0 ; i < kategorijeIzBaze.size(); i++){
                if(kategorijeIzBaze.get(i).getNaziv().equals(nazivKat))
                    katKviza = kategorijeIzBaze.get(i);
            }
            String p = c.getString(kvizPit);
            String pom = "";
            int j = 0;
            ArrayList<Pitanje> pitanja = new ArrayList<>();
            ArrayList<Pair<String, String>> pitanjaIzBazeSaId  = dohvatiPitanjaIzBazeZaKviz();
            for(int i = 0; i < p.length(); i++){
                if(p.charAt(i) == ',' || i == p.length() - 1){
                    if(i == p.length() - 1) i++;
                    pom = p.substring(j, i);
                    String pitanjeString = "";
                    for(int m = 0; m < pitanjaIzBazeSaId.size(); m++){
                        if(pitanjaIzBazeSaId.get(m).first.equals(pom)){
                            pitanjeString = pitanjaIzBazeSaId.get(m).second;
                            System.out.println(pitanjeString);
                            break;
                        }
                    }
                    ArrayList<Pitanje> pitanjaIzBaze = citajPitanjaIzBaze();
                    for(int n = 0; n < pitanjaIzBaze.size(); n++){
                        if(pitanjaIzBaze.get(n).getNaziv().equals(pitanjeString)){
                           pitanja.add(pitanjaIzBaze.get(n));
                           break;
                        }
                    }
                    j = i;
                    j++;
                }
            }
            listaKvizovaIzBaze.add(new Kviz(c.getString(kvizNaziv), pitanja, katKviza));
        }
        return listaKvizovaIzBaze;
    }
    public ArrayList<Pair<String, Pair<Integer, Pair<String, String>>>> citajRangIzBaze(){
        ArrayList<Pair<String, Pair<Integer, Pair<String, String>>>> listaRanglista = new ArrayList<Pair<String, Pair<Integer, Pair<String, String>>>>();
        Cursor c;
        SQLiteDatabase db = this.getReadableDatabase();
        c = db.rawQuery("SELECT * FROM " + DATABASE_TABLE​_RANGLISTA, null);

        //int kat_id = c.getColumnIndexOrThrow(KATEGORIJA_ID);
        int ime  = c.getColumnIndexOrThrow(IGRAC);
        int kvizRG = c.getColumnIndexOrThrow(RG_KVIZ);
        int poz = c.getColumnIndexOrThrow(POZCIJA_IGRACA);
        int proc = c.getColumnIndexOrThrow(PROCENAT);

        while (c.moveToNext()) {
            listaRanglista.add(new Pair<String, Pair<Integer, Pair<String, String>>>(c.getString(kvizRG), new Pair<Integer, Pair<String, String>>(c.getInt(poz), new Pair<String, String>(c.getString(ime),c.getString(proc)))));
            System.out.println("udjeeeeeeeeeeee");

        }
        c.close();
        return listaRanglista;
    }
}