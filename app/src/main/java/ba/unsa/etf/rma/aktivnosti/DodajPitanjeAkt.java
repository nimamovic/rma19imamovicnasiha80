package ba.unsa.etf.rma.aktivnosti;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.common.collect.Lists;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.klase.OdgovoriAdapter;
import ba.unsa.etf.rma.klase.Pitanje;

public class DodajPitanjeAkt extends Activity {
    private ListView lvOdgovori;
    private EditText etNaziv;
    private EditText etOdgovor;
    private Button btnDodajOdgovor;
    private Button btnDodajTacan;
    private Button btnDodajPitanje;

    private ArrayList<Pair<String,Boolean>> odgovori = new ArrayList<Pair<String,Boolean>>();
    private ArrayList<String> odgovoriBezTacnosti = new ArrayList<String>();

    public static ArrayList<Pair<Pitanje, String>> pitanjaIzBaze = new ArrayList<Pair<Pitanje, String>>();

    private OdgovoriAdapter adapterOdg;
    private String tacan;
    private int poz;

    private boolean imaTacan = false;
    private boolean imaLiVecPitanje = false;
    private boolean imaLiVecOdg = false;

    private class KrerirajDokumentTaskZaPitanje extends AsyncTask<String,Void,Void> {
        @Override
        protected Void doInBackground(String... strings){

            GoogleCredential credentials;
            try {

                InputStream is = getResources().openRawResource(R.raw.secret);
                credentials = GoogleCredential.fromStream(is).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                credentials.refreshToken();
                String TOKEN = credentials.getAccessToken();

                Log.d("TOKEN EVO GA",TOKEN);

                String url = "https://firestore.googleapis.com/v1/projects/projekat-20b5a/databases/(default)/documents/Pitanja?access_token=";
                URL urlObj = new URL (url + URLEncoder.encode(TOKEN, "UTF-8"));
                HttpURLConnection connection = (HttpURLConnection) urlObj.openConnection();

                connection.setDoOutput(true);
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setRequestProperty("Accept", "application/json");


                String dokument = "{ \"fields\": { \"naziv\": {\"stringValue\":\""+etNaziv.getText().toString()+"\"} , \"odgovori\" : { \"arrayValue\" : { \"values\" : [";
                for(int i = 0; i < odgovoriBezTacnosti.size(); i++){
                        dokument += "{\"stringValue\" : \"" + odgovoriBezTacnosti.get(i) + "\"}";
                        if (i != odgovoriBezTacnosti.size() - 1) dokument+= ",";
                }
                dokument+= "]}} , \"indexTacnog\": {\"integerValue\":\"" + poz +"\"}}}";
                try (OutputStream os = connection.getOutputStream())
                {
                    byte[] input = dokument.getBytes("utf-8");
                    os.write(input, 0, input.length);
                }

                int code = connection.getResponseCode();
                InputStream odgovor = connection.getInputStream();
                StringBuilder response = null;
                try(BufferedReader br = new BufferedReader (
                        new InputStreamReader(odgovor, "utf-8"))
                ){
                    response = new StringBuilder();
                    String rensponseLine = null;
                    while((rensponseLine = br.readLine()) != null){
                        response.append(rensponseLine.trim());
                    }
                    Log.d("ODGOVOR", response.toString());
                }
                String rezultat = response.toString();
                JSONObject jo = new JSONObject(rezultat);
                String name = jo.getString("name");
                int duzina = name.length();
                String ime = "";
                int brojac = 0;
                for(int j = 0; j < name.length(); j++){
                    if(name.charAt(j) == '/' && brojac!=6){
                        brojac++;
                    }
                    else if(brojac == 6){
                        ime = name.substring(j, duzina);
                        break;
                    }
                }
                JSONObject fields = jo.getJSONObject("fields");
                JSONObject stringValue = fields.getJSONObject("naziv");
                String naziv = stringValue.getString("stringValue");
                JSONObject integerValue = fields.getJSONObject("indexTacnog");
                int indexTacnog = integerValue.getInt("integerValue");

                JSONObject odgovorIzbaze = fields.getJSONObject("odgovori");
                JSONObject arrayValue = odgovorIzbaze.getJSONObject("arrayValue");

                JSONArray values = arrayValue.getJSONArray("values");
                ArrayList<String> listaOdgovora = new ArrayList<>();
                    for(int i = 0; i < values.length(); i++){
                        JSONObject item = values.getJSONObject(i);
                        String odg = item.getString("stringValue");
                        listaOdgovora.add(odg);
                    }
                String odgovorT = "";
                for (int k = 0; k < listaOdgovora.size(); k++) {
                    if (k == indexTacnog) {
                        odgovorT = listaOdgovora.get(k);
                    }
                }

                Pitanje pomP = new Pitanje(naziv, naziv, listaOdgovora, odgovorT);
                pitanjaIzBaze.add(new Pair<Pitanje, String>(pomP, ime));
                KvizoviAkt.svaPitanja.add(new Pair<Pitanje, String>(pomP, ime));
                KvizoviAkt.pitanjaUKvizovima.add(new Pair<Pitanje, String>(pomP,ime));

                    Log.d("ODGOVOR", response.toString());
                }
            catch(JSONException e){
                e.printStackTrace();
            }
            catch (IOException e){
                e.printStackTrace();
            }
            return null;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dodaj_pitanje_akt);
        lvOdgovori = (ListView) findViewById(R.id.lvOdgovori);
        etNaziv = (EditText) findViewById(R.id.etNaziv);
        etOdgovor = (EditText) findViewById(R.id.etOdgovor);
        btnDodajOdgovor = (Button) findViewById(R.id.btnDodajOdgovor);
        btnDodajTacan = (Button) findViewById(R.id.btnDodajTacan);
        btnDodajPitanje = (Button) findViewById(R.id.btnDodajPitanje);

        etNaziv.setText("");
        etOdgovor.setText("");

        adapterOdg = new OdgovoriAdapter(this, odgovori);
        lvOdgovori.setAdapter(adapterOdg);

        btnDodajOdgovor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String uneseniOdg = etOdgovor.getText().toString();
                if (uneseniOdg.length() > 0){
                    imaLiVecOdg = false;
                    int d = odgovori.size();
                    for(int i = 0; i < d ;i++){
                       if(uneseniOdg.equals(odgovori.get(i).first)) imaLiVecOdg = true;
                    }
                    if(!imaLiVecOdg) odgovori.add(new Pair<String, Boolean>(uneseniOdg, false));
                    adapterOdg = new OdgovoriAdapter(DodajPitanjeAkt.this, odgovori);
                    etOdgovor.setText("");
                    lvOdgovori.setAdapter(adapterOdg);
                }
            }
        });

        btnDodajTacan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String uneseniOdg = etOdgovor.getText().toString();
                tacan = uneseniOdg;
                if (uneseniOdg.length() > 0){
                    imaLiVecOdg = false;
                    int d = odgovori.size();
                    for(int i = 0; i < d ;i++){
                        if(uneseniOdg.equals(odgovori.get(i).first)) imaLiVecOdg = true;
                    }
                    if(!imaLiVecOdg) odgovori.add(new Pair<String, Boolean>(uneseniOdg, true));
                    adapterOdg = new OdgovoriAdapter(DodajPitanjeAkt.this, odgovori);
                    etOdgovor.setText("");
                    lvOdgovori.setAdapter(adapterOdg);
                }
                btnDodajTacan.setEnabled(false);
            }
        });

        lvOdgovori.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(odgovori.get(position).second == true) btnDodajTacan.setEnabled(true);
                odgovori.remove(position);
                adapterOdg.notifyDataSetChanged();
            }
        });




        btnDodajPitanje.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isNetworkAvailable()) {
                    Intent returnIntent = getIntent();
                    for (Pair<String, Boolean> s : odgovori) {
                        if (s.second == true) imaTacan = true;
                    }
                    if (etNaziv.getText().toString().length() == 0) {
                        etNaziv.setBackgroundColor(Color.RED);
                    } else if (imaTacan == false) {

                    } else {
                        etNaziv.setBackgroundColor(Color.WHITE);
                        odgovoriBezTacnosti.removeAll(odgovoriBezTacnosti);
                        int pomZaPoz = 0;
                        for (Pair<String, Boolean> s : odgovori) {
                            odgovoriBezTacnosti.add(s.first);
                            if (s.second == true) poz = pomZaPoz;
                            pomZaPoz++;
                        }

                        Pitanje p = new Pitanje(etNaziv.getText().toString(), etNaziv.getText().toString(), odgovoriBezTacnosti, tacan);
                        new KrerirajDokumentTaskZaPitanje().execute();
                        returnIntent.putExtra("PITANJE", p.getNaziv());
                        returnIntent.putExtra("PITANJE2", p.getTekstPitanja());
                        returnIntent.putExtra("ODG", p.getOdgovori());
                        returnIntent.putExtra("TACAN", p.getTacan());
                        setResult(RESULT_OK, returnIntent);
                        finish();
                    }
                    imaLiVecPitanje = false;
                    imaLiVecOdg = false;
                }
            }
        });




    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}
