package ba.unsa.etf.rma.aktivnosti;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Spinner;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.common.collect.Lists;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.fragmenti.DetailFrag;
import ba.unsa.etf.rma.fragmenti.ListaFrag;
import ba.unsa.etf.rma.klase.AsyncResponse;
import ba.unsa.etf.rma.klase.Kategorija;
import ba.unsa.etf.rma.klase.KategorijaAdapter;
import ba.unsa.etf.rma.klase.Kviz;
import ba.unsa.etf.rma.klase.KvizAdapter;
import ba.unsa.etf.rma.klase.KvizoviDBOpenHelper;
import ba.unsa.etf.rma.klase.NetworkUtil;
import ba.unsa.etf.rma.klase.Pitanje;

public class KvizoviAkt extends AppCompatActivity implements ListaFrag.OnItemClick{
    private static final int MY_CAL_REQ = 0;
    Spinner spPostojeceKategorije;
    ListView lvKvizovi;

    public static KvizoviDBOpenHelper kHelper;
    public static SQLiteDatabase baza = null;

    private KategorijaAdapter adapterKat;
    private KvizAdapter adapterKv;


    public static ArrayList<Kviz> kvizovi = new ArrayList<Kviz>();
    public static ArrayList<Kategorija> kategorije = new ArrayList<Kategorija>();

    private ArrayList<Pitanje> pitanja = new ArrayList<Pitanje>();
    private Kategorija svi = new Kategorija("Svi","0");
    private int minute;
    private int x;

    private ArrayList<Pair<String, Pair<String, String>>> dogadaji = new ArrayList<Pair<String, Pair<String, String>>>();

    public static ArrayList<Kviz> kviz = new ArrayList<Kviz>();
    public static ArrayList<Pair<Kategorija, String>> sveKategorije = new ArrayList<Pair<Kategorija, String>>();
    public static ArrayList<Pair<Pitanje, String>> svaPitanja = new ArrayList<>();
    public static ArrayList<Pair<Kviz, String>> sviKvizovi = new ArrayList<>();
    public static ArrayList<Pair<String, Pair<Integer,Pair<String, String>>>> sveRangListePrviPut = new ArrayList<>();
    public static ArrayList<Pair<Pitanje,String>> pitanjaUKvizovima = new ArrayList<>();
    public static ArrayList<Kviz> kvizoviPremaKategoriji= new ArrayList<Kviz>();
    public static  String kategorijaZaFiltriranjeBaze = "";
    private Kviz pom;
    public static boolean izabranoSvi = false;
    public static boolean imaSvi = false;
    public static int pozicija;

    public static int index;
    public static boolean nijeKliknuto = true;
    public static String kojaJeIzabrana = "Svi";

    private String nazivK2;
    private String ime2;
    private int redniBrojPozcije2 = 0;
    private String procenat2;
    public static ArrayList<Pair<String, Pair<Integer, Pair<String, String>>>> rangListeBezInterneta = new ArrayList<>();

    public class KrerirajDokumentTask extends AsyncTask<String,Void,Void> {
        AsyncResponse ar = null;

        public KrerirajDokumentTask(AsyncResponse asyncResponse) {
            this.ar = asyncResponse;
        }

        @Override
        protected Void doInBackground(String... strings){

            GoogleCredential credentials;
            try {
                kvizovi.clear();
                kvizovi.add(new Kviz("Dodaj kviz", pitanja, svi));
                kategorije.clear();
                kategorije.add(svi);
                sveRangListePrviPut.clear();
                sviKvizovi.clear();
                sveKategorije.clear();
                svaPitanja.clear();
                InputStream is = getResources().openRawResource(R.raw.secret);
                credentials = GoogleCredential.fromStream(is).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                credentials.refreshToken();
                String TOKEN = credentials.getAccessToken();

                Log.d("TOKEN EVO GA", TOKEN);

                String url1 = "https://firestore.googleapis.com/v1/projects/projekat-20b5a/databases/(default)/documents/Kategorije?access_token=";
                URL urlObj = new URL(url1 + URLEncoder.encode(TOKEN, "UTF-8"));
                HttpURLConnection connection1 = (HttpURLConnection) urlObj.openConnection();
                /*connection.setDoOutput(true);
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setRequestProperty("Accept", "application/json");*/
                connection1.connect();


                // String dokument = "{ \"fields\": { \"atribut\": {\"stringValue\":\"novi dokument\"}}}";
                /*try (OutputStream os = connection1.getOutputStream())
                {
                    byte[] input = dokument.getBytes("utf-8");
                    os.write(input, 0, input.length);
                }*/
                InputStream odgovorKategorija = connection1.getInputStream();
                StringBuilder response1 = null;
                try (BufferedReader br = new BufferedReader(
                        new InputStreamReader(odgovorKategorija, "utf-8"))
                ) {
                    response1 = new StringBuilder();
                    String rensponseLine = null;
                    while ((rensponseLine = br.readLine()) != null) {
                        response1.append(rensponseLine.trim());
                    }
                    Log.d("ODGOVOR ZA KATEGORIJU ", response1.toString());
                }
                String katy = response1.toString();
                if (!katy.equals("{}")) {
                    JSONObject jo = new JSONObject(katy);
                    JSONArray documents = jo.getJSONArray("documents");
                    for (int i = 0; i < documents.length(); i++) {
                        JSONObject documentsK = documents.getJSONObject(i);
                        JSONObject fields = documentsK.getJSONObject("fields");
                        JSONObject jsonNaziva = fields.getJSONObject("naziv");
                        String naziv = jsonNaziva.getString("stringValue");
                        JSONObject jsonIkone = fields.getJSONObject("idIkonice");
                        int idIkonice = jsonIkone.getInt(("integerValue"));
                        String id = documentsK.getString("name");
                        int duzina = id.length();
                        String ime = "";
                        int brojac = 0;
                        for(int j = 0; j < id.length(); j++){
                            if(id.charAt(j) == '/' && brojac!=6){
                                brojac++;
                            }
                            else if(brojac == 6){
                                ime = id.substring(j, duzina);
                                break;
                            }
                        }
                        String ik = String.valueOf(idIkonice);
                        Kategorija pomK = new Kategorija(naziv, ik);
                        sveKategorije.add(new Pair<Kategorija, String>(pomK, ime));
                        kategorije.add(pomK);
                    }
                }
                String url2 = "https://firestore.googleapis.com/v1/projects/projekat-20b5a/databases/(default)/documents/Pitanja?access_token=";
                URL urlObj2 = new URL (url2 + URLEncoder.encode(TOKEN, "UTF-8"));
                HttpURLConnection connection = (HttpURLConnection) urlObj2.openConnection();
                connection.connect();
                InputStream odgovor = connection.getInputStream();
                StringBuilder response = null;
                try(BufferedReader br = new BufferedReader (
                        new InputStreamReader(odgovor, "utf-8"))
                ){
                    response = new StringBuilder();
                    String rensponseLine = null;
                    while((rensponseLine = br.readLine()) != null){
                        response.append(rensponseLine.trim());
                    }
                    Log.d("ODGOVOR ZA PITANJA", response.toString());
                }
                String rezultat = response.toString();
                if(!rezultat.equals("{}")) {
                    JSONObject jo = new JSONObject(rezultat);
                    JSONArray documentsNiz = jo.getJSONArray("documents");
                    for (int p = 0; p < documentsNiz.length(); p++) {
                        JSONObject documentsO = documentsNiz.getJSONObject(p);
                        String name = documentsO.getString("name");
                        int duzina = name.length();
                        String ime = "";
                        int brojac = 0;
                        for(int j = 0; j < name.length(); j++){
                            if(name.charAt(j) == '/' && brojac!=6){
                                brojac++;
                            }
                            else if(brojac == 6){
                                ime = name.substring(j, duzina);
                                break;
                            }
                        }
                        JSONObject fields = documentsO.getJSONObject("fields");
                        JSONObject stringValue = fields.getJSONObject("naziv");
                        String naziv = stringValue.getString("stringValue");
                        JSONObject integerValue = fields.getJSONObject("indexTacnog");
                        int indexTacnog = integerValue.getInt("integerValue");
                        JSONObject odgovorIzbaze = fields.getJSONObject("odgovori");
                        JSONObject arrayValue = odgovorIzbaze.getJSONObject("arrayValue");
                        JSONArray values = arrayValue.getJSONArray("values");
                        ArrayList<String> listaOdgovora = new ArrayList<>();
                        for (int i = 0; i < values.length(); i++) {
                            JSONObject item = values.getJSONObject(i);
                            String odg = item.getString("stringValue");
                            listaOdgovora.add(odg);
                        }
                        String odgovorT = "";
                        for (int k = 0; k < listaOdgovora.size(); k++) {
                            if (k == indexTacnog) {
                                odgovorT = listaOdgovora.get(k);
                            }
                        }
                        Pitanje pomP = new Pitanje(naziv, odgovorT, listaOdgovora, odgovorT);
                        svaPitanja.add(new Pair<Pitanje, String>(pomP, ime));
                    }
                }

                String url3 ="https://firestore.googleapis.com/v1/projects/projekat-20b5a/databases/(default)/documents/Kvizovi?access_token=";
                URL urlObj3 = new URL (url3 + URLEncoder.encode(TOKEN, "UTF-8"));
                HttpURLConnection connection3 = (HttpURLConnection) urlObj3.openConnection();
                connection3.connect();
                InputStream odgovor3 = connection3.getInputStream();
                StringBuilder response3 = null;
                try(BufferedReader br = new BufferedReader (
                        new InputStreamReader(odgovor3, "utf-8"))
                ){
                    response3 = new StringBuilder();
                    String rensponseLine = null;
                    while((rensponseLine = br.readLine()) != null){
                        response3.append(rensponseLine.trim());
                    }
                    Log.d("ODGOVOR ZA KVIZOVE ", response3.toString());
                }
                String rezultatKvizovi = response3.toString();
                if(!rezultatKvizovi.equals("{}")) {
                    JSONObject joKviz = new JSONObject(rezultatKvizovi);
                    JSONArray documentsKA = joKviz.getJSONArray("documents");
                    for (int i = 0; i < documentsKA.length(); i++) {
                        ArrayList<String> idPitanja = new ArrayList<>();
                        JSONObject docKO = documentsKA.getJSONObject(i);
                        String name = docKO.getString("name");
                        int duzina = name.length();
                        String ime = "";
                        int brojac = 0;
                        for(int j = 0; j < name.length(); j++){
                            if(name.charAt(j) == '/' && brojac!=6){
                                brojac++;
                            }
                            else if(brojac == 6){
                                ime = name.substring(j, duzina);
                                break;
                            }
                        }
                        JSONObject fields = docKO.getJSONObject("fields");
                        JSONObject stringValue = fields.getJSONObject("naziv");
                        String nazivK = stringValue.getString("stringValue");

                        JSONObject referenceValue = fields.getJSONObject("idKategorije");
                        String idKategorijeZaKviz = referenceValue.getString("stringValue");
                        Kategorija katK = new Kategorija();
                        for (int j = 0; j < sveKategorije.size(); j++) {
                            if (idKategorijeZaKviz.equals(sveKategorije.get(j).second)) {
                                katK.setNaziv(sveKategorije.get(j).first.getNaziv());
                                katK.setId(sveKategorije.get(j).first.getId());
                            }
                        }
                        JSONObject pitanjaF = fields.getJSONObject("pitanja");
                        JSONObject arrayValue = pitanjaF.getJSONObject("arrayValue");
                        ArrayList<Pitanje> pitanjaKviza = new ArrayList<>();
                        if (!arrayValue.toString().equals("{}")) {
                            JSONArray values = arrayValue.getJSONArray("values");
                            for (int k = 0; k < values.length(); k++) {
                                JSONObject item = values.getJSONObject(k);
                                String pitK = item.getString("stringValue");
                                idPitanja.add(pitK);
                            }
                            for (int l = 0; l < svaPitanja.size(); l++) {
                                for (int m = 0; m < idPitanja.size(); m++) {
                                    if (idPitanja.get(m).equals(svaPitanja.get(l).second)) {
                                        pitanjaKviza.add(svaPitanja.get(l).first);
                                        pitanjaUKvizovima.add(svaPitanja.get(l));
                                    }
                                }
                            }
                        }
                        Kviz pomK = new Kviz(nazivK, pitanjaKviza, katK);
                        kvizovi.add( kvizovi.size() - 1,pomK);
                        sviKvizovi.add(new Pair<Kviz, String> (pomK, ime));
                    }
                }

                sveRangListePrviPut.clear();
                String url4 = "https://firestore.googleapis.com/v1/projects/projekat-20b5a/databases/(default)/documents/Rangliste?access_token=";
                URL urlObj4 = new URL (url4 + URLEncoder.encode(TOKEN, "UTF-8"));
                HttpURLConnection connection4 = (HttpURLConnection) urlObj4.openConnection();
                connection4.connect();
                InputStream odgovor4 = connection4.getInputStream();
                StringBuilder response4 = null;
                try(BufferedReader br = new BufferedReader (
                        new InputStreamReader(odgovor4, "utf-8"))
                ){
                    response4 = new StringBuilder();
                    String rensponseLine = null;
                    while((rensponseLine = br.readLine()) != null){
                        response4.append(rensponseLine.trim());
                    }
                    Log.d("ODGOVOR ZA PITANJA", response4.toString());
                }
                String rezultatR = response4.toString();
                if (!rezultatR.equals("{}")) {
                    JSONObject jo = new JSONObject(rezultatR);
                    JSONArray documents = jo.getJSONArray("documents");
                    for (int i = 0; i < documents.length(); i++) {
                        JSONObject doc = documents.getJSONObject(i);
                        String name = doc.getString("name");
                        JSONObject fields = doc.getJSONObject("fields");
                        JSONObject stringValue = fields.getJSONObject("nazivKviza");
                        String naziv = stringValue.getString("stringValue");

                        JSONObject map = fields.getJSONObject("lista");
                        JSONObject mapValue = map.getJSONObject("mapValue");

                        JSONObject fieldsM = mapValue.getJSONObject("fields");
                        JSONObject integerValue = fieldsM.getJSONObject("pozicija");
                        int rangiran = integerValue.getInt("integerValue");
                        JSONObject map2 = fieldsM.getJSONObject("mapa");
                        JSONObject mapValue2 = map2.getJSONObject("mapValue");
                        JSONObject fieldsM2 = mapValue2.getJSONObject("fields");
                        JSONObject stringValue2 = fieldsM2.getJSONObject("imeIgraca");
                        String ime = stringValue2.getString("stringValue");
                        JSONObject integerValueProcenat = fieldsM2.getJSONObject("procenat");
                        String procenat = integerValueProcenat.getString("stringValue");

                        Pair<String, String> mapaPom = new Pair<>(ime, procenat);
                        Pair<Integer, Pair<String, String>> mapaPrava = new Pair<>(rangiran, mapaPom);
                        sveRangListePrviPut.add(new Pair<>(naziv, mapaPrava));
                    }
                }

            }
            catch (JSONException e){
                e.printStackTrace();
            }
            catch (IOException e){
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void string) {
            ar.processFinish();
        }
    }
    public class KrerirajDokumentRang2 extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... strings) {

            try {
                InputStream is = getResources().openRawResource(R.raw.secret);
                GoogleCredential credentials = GoogleCredential.fromStream(is).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                credentials.refreshToken();
                String TOKEN = credentials.getAccessToken();
                System.out.println("SADA JE TOKEN: " + TOKEN);
                String url = "https://firestore.googleapis.com/v1/projects/projekat-20b5a/databases/(default)/documents/Rangliste?access_token=";
                URL urlObj = new URL(url + URLEncoder.encode(TOKEN, "UTF-8"));
                HttpURLConnection connection = (HttpURLConnection) urlObj.openConnection();
                connection.setDoOutput(true);
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setRequestProperty("Accept", "application/json");

                String dokument = "{\"fields\" : { \"nazivKviza\" : { \"stringValue\" : \"" + nazivK2 + "\" }, \"lista\" : { \"mapValue\" : { \"fields\" : { \"pozicija\" : { \"integerValue\" : \"" + redniBrojPozcije2
                        + "\" }, \"mapa\" : { \"mapValue\" : { \"fields\" : { \"procenat\" : { \"stringValue\" : \"" + procenat2 + "\" }, \"imeIgraca\" : { \"stringValue\" : \""
                        + ime2 + "\" } } } } } } } } }";
                try (OutputStream os = connection.getOutputStream()) {
                    byte[] input = dokument.getBytes("utf-8");
                    os.write(input, 0, input.length);
                }

                int code = connection.getResponseCode();
                InputStream odgovor = connection.getInputStream();
                StringBuilder response = null;
                try (BufferedReader br = new BufferedReader(
                        new InputStreamReader(odgovor, "utf-8"))
                ) {
                    response = new StringBuilder();
                    String rensponseLine = null;
                    while ((rensponseLine = br.readLine()) != null) {
                        response.append(rensponseLine.trim());
                    }
                    Log.d("ODGOVOR", response.toString());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public class KrerirajDokumentTaskFiltriranje extends AsyncTask<String,Void,Void> {
        AsyncResponse ar = null;

        public KrerirajDokumentTaskFiltriranje(AsyncResponse asyncResponse) {
            this.ar = asyncResponse;
        }

        @Override
        protected Void doInBackground(String... strings){

            GoogleCredential credentials;
            try {

                InputStream is = getResources().openRawResource(R.raw.secret);
                credentials = GoogleCredential.fromStream(is).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                credentials.refreshToken();
                String TOKEN = credentials.getAccessToken();

                Log.d("TOKEN EVO GA", TOKEN);

                String query = "{\n" +
                        "\"structuredQuery\": {\n" +
                        "\"where\": {\n" +
                        "\"fieldFilter\": {\n" +
                        "\"field\": {\"fieldPath\": \"idKategorije\"}, \n" +
                        "\"op\": \"EQUAL\",\n" +
                        "\"value\": {\"stringValue\": \"" + kategorijaZaFiltriranjeBaze + "\"}\n" +
                        "}\n" +
                        "},\n" +
                        "\"select\": {\"fields\": [ {\"fieldPath\": \"idKategorije\"}, {\"fieldPath\": \"naziv\"}, {\"fieldPath\": \"pitanja\"} ] }, \n" +
                        "\"from\": [{\"collectionId\" : \"Kvizovi\"}], \n" +
                        "\"limit\" : 1000\n" +
                        "}\n" +
                        "}";

                String url = "https://firestore.googleapis.com/v1/projects/projekat-20b5a/databases/(default)/documents:runQuery?access_token=";
                URL urlObj = new URL(url + URLEncoder.encode(TOKEN, "UTF-8"));
                HttpURLConnection connection = (HttpURLConnection) urlObj.openConnection();
                connection.setDoOutput(true);
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setRequestProperty("Accept", "application/json");



                try (OutputStream os = connection.getOutputStream()) {
                    byte[] input = query.getBytes("utf-8");
                    os.write(input, 0, input.length);
                }

                int code = connection.getResponseCode();
                StringBuilder response = null;
                InputStream odgovor = connection.getInputStream();
                try (BufferedReader br = new BufferedReader(
                        new InputStreamReader(odgovor, "utf-8"))
                ) {
                    response = new StringBuilder();
                    String rensponseLine = null;
                    while ((rensponseLine = br.readLine()) != null) {
                        response.append(rensponseLine.trim());
                    }
                    Log.d("ODGOVORRRRRRRRR", response.toString());
                }

                String rezultat = response.toString();
                rezultat = "{ \"documents\": " + rezultat + "}";
                if(!rezultat.equals("{}")) {
                    JSONObject joKviz = new JSONObject(rezultat);
                    JSONArray documentsKA = joKviz.getJSONArray("documents");
                    for (int i = 0; i < documentsKA.length(); i++) {
                        ArrayList<String> idPitanja = new ArrayList<>();
                        JSONObject docKO = documentsKA.getJSONObject(i);
                        if (docKO.has("document")){
                            JSONObject doc = docKO.getJSONObject("document");
                            String name = doc.getString("name");
                            int duzina = name.length();
                            String ime = "";
                            int brojac = 0;
                            for (int j = 0; j < name.length(); j++) {
                                if (name.charAt(j) == '/' && brojac != 6) {
                                    brojac++;
                                } else if (brojac == 6) {
                                    ime = name.substring(j, duzina);
                                    break;
                                }
                            }
                            JSONObject fields = doc.getJSONObject("fields");
                            JSONObject stringValue = fields.getJSONObject("naziv");
                            String nazivK = stringValue.getString("stringValue");

                            JSONObject referenceValue = fields.getJSONObject("idKategorije");
                            String idKategorijeZaKviz = referenceValue.getString("stringValue");
                            Kategorija katK = new Kategorija();
                            for (int j = 0; j < sveKategorije.size(); j++) {
                                if (idKategorijeZaKviz.equals(sveKategorije.get(j).second)) {
                                    katK.setNaziv(sveKategorije.get(j).first.getNaziv());
                                    katK.setId(sveKategorije.get(j).first.getId());
                                }
                            }
                            JSONObject pitanjaF = fields.getJSONObject("pitanja");
                            JSONObject arrayValue = pitanjaF.getJSONObject("arrayValue");
                            ArrayList<Pitanje> pitanjaKviza = new ArrayList<>();
                            if (!arrayValue.toString().equals("{}")) {
                                JSONArray values = arrayValue.getJSONArray("values");
                                for (int k = 0; k < values.length(); k++) {
                                    JSONObject item = values.getJSONObject(k);
                                    String pitK = item.getString("stringValue");
                                    idPitanja.add(pitK);
                                }
                                for (int l = 0; l < svaPitanja.size(); l++) {
                                    for (int m = 0; m < idPitanja.size(); m++) {
                                        if (idPitanja.get(m).equals(svaPitanja.get(l).second)) {
                                            pitanjaKviza.add(svaPitanja.get(l).first);
                                        }
                                    }
                                }
                            }
                            Kviz pomK = new Kviz(nazivK, pitanjaKviza, katK);
                            kvizoviPremaKategoriji.add(pomK);

                        }
                    }
                }
            }
            catch (JSONException e){
                e.printStackTrace();
            }
            catch (IOException e){
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void string) {
            ar.processFinish();
        }
    }

    boolean receiverImaNet = true;

    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int status = NetworkUtil.getConnectivityStatusString(context);
            if ("android.net.conn.CONNECTIVITY_CHANGE".equals(intent.getAction())) {
                if (status == NetworkUtil.NETWORK_STATUS_NOT_CONNECTED) {
                   // kHelper.brisi();
                   // upisivanjeKadNemaInterenta();
                    kategorije.clear();
                    kvizovi.clear();
                    if(kategorije.size() == 0)kategorije.add(svi);
                    kategorije.addAll(kHelper.citajKategorijeIzBaze());
                    kvizovi.add(new Kviz("Dodaj kviz", pitanja, svi));
                    kvizovi.addAll(kvizovi.size() - 1,kHelper.citajKvizoveIzBaze());
                    adapterKat = new KategorijaAdapter(KvizoviAkt.this, kategorije);
                    spPostojeceKategorije.setAdapter(adapterKat);
                    adapterKv = new KvizAdapter(KvizoviAkt.this, kvizovi);
                    lvKvizovi.setAdapter(adapterKv);
                    adapterKat.notifyDataSetChanged();
                    adapterKv.notifyDataSetChanged();
                    receiverImaNet = false;
                    Log.d("INTERNET", "---SADA NEMA INTNERNETA---");
                } else {
                    Log.d("INTERNET", "---SADA IMAM INTERNETA---");
                    if(rangListeBezInterneta.size() != 0){
                        for(int i = 0; i <rangListeBezInterneta.size(); i++){
                            ime2 = rangListeBezInterneta.get(i).second.second.first;
                            procenat2 = rangListeBezInterneta.get(i).second.second.second;
                            nazivK2 = rangListeBezInterneta.get(i).first;
                            new KrerirajDokumentRang2().execute();
                        }
                        rangListeBezInterneta.clear();
                    }
                    kvizovi.clear();
                    kvizovi.add(new Kviz("Dodaj kviz", pitanja, svi));
                    new KrerirajDokumentTask(new AsyncResponse() {
                        @Override
                        public void processFinish() {
                            adapterKat = new KategorijaAdapter(KvizoviAkt.this, kategorije);
                            spPostojeceKategorije.setAdapter(adapterKat);
                            adapterKv = new KvizAdapter(KvizoviAkt.this, kvizovi);
                            lvKvizovi.setAdapter(adapterKv);
                            kHelper.brisi();
                            upisivanjeKadNemaInterenta();
                            adapterKat.notifyDataSetChanged();
                            adapterKv.notifyDataSetChanged();
                        }
                    }).execute();
                    receiverImaNet = true;

                }
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kvizovi_akt);


        kHelper = new KvizoviDBOpenHelper( this);
        try{
            baza = kHelper.getWritableDatabase();
        }
        catch (SQLException e){
            baza = kHelper.getReadableDatabase();
        }


        IntentFilter intent = new IntentFilter();
        intent.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        registerReceiver(receiver, intent);


            lvKvizovi = (ListView) findViewById(R.id.lvKvizovi);
            if (lvKvizovi != null) {
                spPostojeceKategorije = (Spinner) findViewById(R.id.spPostojeceKategorije);

                for (int i = 0; i < sveKategorije.size(); i++) {
                    boolean imaTaKategorija = false;
                    for (int j = 0; j < DodajKategorijuAkt.kategorijeIzBaze.size(); j++) {
                        if (sveKategorije.get(i).first.getNaziv().equals(DodajKategorijuAkt.kategorijeIzBaze.get(j).first.getNaziv())) {
                            imaTaKategorija = true;
                            break;
                        }
                    }
                    if (!imaTaKategorija)
                        DodajKategorijuAkt.kategorijeIzBaze.add(sveKategorije.get(i));
                }

                //  if(sveKategorije.size() != 0) kategorijaZaFiltriranjeBaze = sveKategorije.get(0).second;


                adapterKat = new KategorijaAdapter(this, kategorije);
                spPostojeceKategorije.setAdapter(adapterKat);
                adapterKat.notifyDataSetChanged();
                adapterKat.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                //      spPostojeceKategorije.setAdapter(adapterKat);
                // spPostojeceKategorije.setFocusable(true);
                //spPostojeceKategorije.setFocusableInTouchMode(true);

                // lvKvizovi = (ListView) findViewById(R.id.lvKvizovi);


                adapterKv = new KvizAdapter(KvizoviAkt.this, kvizovi);
                lvKvizovi.setAdapter(adapterKv);
                adapterKv.notifyDataSetChanged();
                //    spPostojeceKategorije.setSelected(false);
                //   spPostojeceKategorije.setSelection(0, true);

                spPostojeceKategorije.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        nijeKliknuto = false;
                        izabranoSvi = false;
                        index = position;
                        System.out.println("indexxxxxxxx" + String.valueOf(index));
                        if (kategorije.get(index).getNaziv().equals("Svi")) {
                            kojaJeIzabrana = "Svi";
                            System.out.println("ušloooooohohohooohoo" + String.valueOf(kategorije.size()));
                            if(isNetworkAvailable()) {
                                new KrerirajDokumentTask(new AsyncResponse() {
                                    @Override
                                    public void processFinish() {
                                        adapterKv = new KvizAdapter(KvizoviAkt.this, kvizovi);
                                        lvKvizovi.setAdapter(adapterKv);
                                        adapterKv.notifyDataSetChanged();
                                    }
                                }).execute();
                            }
                            else{
                                kviz = new ArrayList<Kviz>();
                                for (Kviz q : kvizovi) kviz.add(q);
                                adapterKv = new KvizAdapter(KvizoviAkt.this, kviz);
                                lvKvizovi.setAdapter(adapterKv);
                                adapterKv.notifyDataSetChanged();
                            }

                        } else {
                            if (isNetworkAvailable()) {
                                kojaJeIzabrana = kategorije.get(index).getNaziv();
                                for (int p = 0; p < sveKategorije.size(); p++) {
                                    if (sveKategorije.get(p).first.getNaziv().equals(kategorije.get(index).getNaziv())) {
                                        index = p;
                                        break;
                                    }
                                }
                                Kategorija k = sveKategorije.get(index).first;
                                for (int i = 0; i < sveKategorije.size(); i++) {
                                    if (k.getNaziv().equals(sveKategorije.get(i).first.getNaziv()))
                                        kategorijaZaFiltriranjeBaze = sveKategorije.get(i).second;
                                }

                                new KrerirajDokumentTaskFiltriranje(new AsyncResponse() {
                                    @Override
                                    public void processFinish() {
                                        kviz = new ArrayList<Kviz>();
                                        // if(!izabranoSvi) {
                                        for (Kviz q : kvizoviPremaKategoriji) kviz.add(q);
                                        kvizoviPremaKategoriji.clear();
                                        System.out.println("evo gagagagagga     " + kategorijaZaFiltriranjeBaze);
                                        //    if (!IgrajKvizAkt.vracaSeIzIgre)
                                        if (!kviz.contains(new Kviz("Dodaj kviz", pitanja, svi)))
                                            kviz.add(new Kviz("Dodaj kviz", pitanja, svi));
                                        adapterKv = new KvizAdapter(KvizoviAkt.this, kviz);
                                        lvKvizovi.setAdapter(adapterKv);
                                        adapterKv.notifyDataSetChanged();
                                    }
                                }).execute();
                            }
                        else{
                            kviz = new ArrayList<Kviz>();
                            for (int i = 0; i < kvizovi.size(); i++) {
                                if (kvizovi.get(i).getKategorija().getNaziv().equals(kategorije.get(index).getNaziv())) {
                                    kviz.add(kvizovi.get(i));
                                }
                            }
                            kviz.add(new Kviz("Dodaj kviz", pitanja, svi));
                            adapterKv = new KvizAdapter(KvizoviAkt.this, kviz);
                            lvKvizovi.setAdapter(adapterKv);
                            adapterKv.notifyDataSetChanged();
                        }
                        }

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> arg0) {

                        nijeKliknuto = false;
                        izabranoSvi = false;
                        index = 0;
                        System.out.println("indexxxxxxxx" + String.valueOf(index));
                        if (kategorije.get(index).getNaziv().equals("Svi")) {
                            kojaJeIzabrana = "Svi";
                            System.out.println("ušloooooohohohooohoo" + String.valueOf(kategorije.size()));
                            if(isNetworkAvailable()) {
                                new KrerirajDokumentTask(new AsyncResponse() {
                                    @Override
                                    public void processFinish() {
                                        adapterKv = new KvizAdapter(KvizoviAkt.this, kvizovi);
                                        lvKvizovi.setAdapter(adapterKv);
                                        adapterKv.notifyDataSetChanged();
                                    }
                                }).execute();
                            }
                            else{
                                kviz = new ArrayList<Kviz>();
                                for (Kviz q : kvizovi) kviz.add(q);
                                adapterKv = new KvizAdapter(KvizoviAkt.this, kviz);
                                lvKvizovi.setAdapter(adapterKv);
                                adapterKv.notifyDataSetChanged();
                            }

                        } else {
                            if (isNetworkAvailable()) {
                                kojaJeIzabrana = kategorije.get(index).getNaziv();
                                for (int p = 0; p < sveKategorije.size(); p++) {
                                    if (sveKategorije.get(p).first.getNaziv().equals(kategorije.get(index).getNaziv())) {
                                        index = p;
                                        break;
                                    }
                                }
                                Kategorija k = sveKategorije.get(index).first;
                                for (int i = 0; i < sveKategorije.size(); i++) {
                                    if (k.getNaziv().equals(sveKategorije.get(i).first.getNaziv()))
                                        kategorijaZaFiltriranjeBaze = sveKategorije.get(i).second;
                                }

                                new KrerirajDokumentTaskFiltriranje(new AsyncResponse() {
                                    @Override
                                    public void processFinish() {
                                        kviz = new ArrayList<Kviz>();
                                        // if(!izabranoSvi) {
                                        for (Kviz q : kvizoviPremaKategoriji) kviz.add(q);
                                        kvizoviPremaKategoriji.clear();
                                        System.out.println("evo gagagagagga     " + kategorijaZaFiltriranjeBaze);
                                        //    if (!IgrajKvizAkt.vracaSeIzIgre)
                                        if (!kviz.contains(new Kviz("Dodaj kviz", pitanja, svi)))
                                            kviz.add(new Kviz("Dodaj kviz", pitanja, svi));
                                        adapterKv = new KvizAdapter(KvizoviAkt.this, kviz);
                                        lvKvizovi.setAdapter(adapterKv);
                                        adapterKv.notifyDataSetChanged();
                                    }
                                }).execute();
                            }
                            else{
                                kviz = new ArrayList<Kviz>();
                                for (int i = 0; i < kvizovi.size(); i++) {
                                    if (kvizovi.get(i).getKategorija().getNaziv().equals(kategorije.get(index).getNaziv())) {
                                        kviz.add(kvizovi.get(i));
                                    }
                                }
                                kviz.add(new Kviz("Dodaj kviz", pitanja, svi));
                                adapterKv = new KvizAdapter(KvizoviAkt.this, kviz);
                                lvKvizovi.setAdapter(adapterKv);
                                adapterKv.notifyDataSetChanged();
                            }
                        }

                    }
                });

                lvKvizovi.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        if (kviz.size() == 0) {
                            if (position == kvizovi.size() - 1) {
                                // Intent myIntent = new Intent(KvizoviAkt.this, DodajKvizAkt.class);
                                // KvizoviAkt.this.startActivityForResult(myIntent, 0);
                                // startActivity(myIntent);
                            } else {
                                getDataFromEventTable(view);
                                if (dogadaji.size() != 0) {
                                    minute = minuteDoEventa(razlikaDoNajblizeg(dogadaji));
                                    x = izracunajX(kvizovi.get(position).getPitanja().size(), 2);
                                    if (minute < x) {
                                        minute++;
                                        //STVARNO NE ZNAM DA LI DA POVEĆAM MINUTE ILI NE NEYYY
                                        AlertDialog.Builder builder1 = new AlertDialog.Builder(KvizoviAkt.this);
                                        builder1.setMessage("Imate događaj u kalendaru za " + minute + " minuta!");
                                        builder1.setCancelable(true);

                                        builder1.setPositiveButton(
                                                "OK",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        dialog.cancel();
                                                    }
                                                });


                                        AlertDialog alert11 = builder1.create();
                                        alert11.show();
                                    } else {
                                        Intent myIntent = new Intent(KvizoviAkt.this, IgrajKvizAkt.class);
                                        myIntent.putExtra("naziv", kvizovi.get(position).getNaziv());
                                        myIntent.putExtra("listapitanja", kvizovi.get(position).getPitanja());
                                        myIntent.putExtra("kategorija", kvizovi.get(position).getKategorija());
                                        KvizoviAkt.this.startActivityForResult(myIntent, 2);
                                    }
                                } else {
                                    Intent myIntent = new Intent(KvizoviAkt.this, IgrajKvizAkt.class);
                                    myIntent.putExtra("naziv", kvizovi.get(position).getNaziv());
                                    myIntent.putExtra("listapitanja", kvizovi.get(position).getPitanja());
                                    myIntent.putExtra("kategorija", kvizovi.get(position).getKategorija());
                                    KvizoviAkt.this.startActivityForResult(myIntent, 2);
                                }
                                //startActivity(myIntent);
                            }
                        } else {
                            if (position == kviz.size() - 1) {
                                //  Intent myIntent = new Intent(KvizoviAkt.this, DodajKvizAkt.class);
                                // KvizoviAkt.this.startActivityForResult(myIntent, 0);
                                //startActivity(myIntent);
                            }
                                else{
                                    getDataFromEventTable(view);
                                    if (dogadaji.size() != 0) {
                                        minute = minuteDoEventa(razlikaDoNajblizeg(dogadaji));
                                        x = izracunajX(kvizovi.get(position).getPitanja().size(), 2);
                                        if (minute < x) {
                                            minute++;
                                            //STVARNO NE ZNAM DA LI DA POVEĆAM MINUTE ILI NE NEYYY
                                            AlertDialog.Builder builder1 = new AlertDialog.Builder(KvizoviAkt.this);
                                            builder1.setMessage("Imate događaj u kalendaru za " + minute + " minuta!");
                                            builder1.setCancelable(true);

                                            builder1.setPositiveButton(
                                                    "OK",
                                                    new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog, int id) {
                                                            dialog.cancel();
                                                        }
                                                    });


                                            AlertDialog alert11 = builder1.create();
                                            alert11.show();
                                        } else {
                                            Intent myIntent = new Intent(KvizoviAkt.this, IgrajKvizAkt.class);
                                            myIntent.putExtra("naziv", kviz.get(position).getNaziv());
                                            myIntent.putExtra("listapitanja", kviz.get(position).getPitanja());
                                            myIntent.putExtra("kategorija", kviz.get(position).getKategorija());
                                            KvizoviAkt.this.startActivityForResult(myIntent, 2);
                                            //startActivity(myIntent);

                                        }
                                    } else {
                                        Intent myIntent = new Intent(KvizoviAkt.this, IgrajKvizAkt.class);
                                        myIntent.putExtra("naziv", kviz.get(position).getNaziv());
                                        myIntent.putExtra("listapitanja", kviz.get(position).getPitanja());
                                        myIntent.putExtra("kategorija", kviz.get(position).getKategorija());
                                        KvizoviAkt.this.startActivityForResult(myIntent, 2);
                                        //startActivity(myIntent);

                                    }
                                }
                            }
                        }
                });
                lvKvizovi.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                        if (kviz.size() == 0) {
                            if (position == kvizovi.size() - 1) {
                                Intent myIntent = new Intent(KvizoviAkt.this, DodajKvizAkt.class);
                                KvizoviAkt.this.startActivityForResult(myIntent, 0);
                            } else {
                                adapterKv.notifyDataSetChanged();
                                Intent myIntent = new Intent(KvizoviAkt.this, DodajKvizAkt.class);
                                myIntent.putExtra("naziv", kvizovi.get(position).getNaziv());
                                myIntent.putExtra("listapitanja", kvizovi.get(position).getPitanja());
                                myIntent.putExtra("kategorija", kvizovi.get(position).getKategorija());
                                pozicija = position;
                                //startActivity(myIntent);
                                KvizoviAkt.this.startActivityForResult(myIntent, 1);
                            }
                        } else {
                            if (position == kviz.size() - 1) {
                                Intent myIntent = new Intent(KvizoviAkt.this, DodajKvizAkt.class);
                                KvizoviAkt.this.startActivityForResult(myIntent, 0);
                            } else {
                                adapterKv.notifyDataSetChanged();
                                Intent myIntent = new Intent(KvizoviAkt.this, DodajKvizAkt.class);
                                myIntent.putExtra("naziv", kviz.get(position).getNaziv());
                                myIntent.putExtra("listapitanja", kviz.get(position).getPitanja());
                                myIntent.putExtra("kategorija", kviz.get(position).getKategorija());
                                pozicija = position;
                                KvizoviAkt.this.startActivityForResult(myIntent, 1);
                                // startActivity(myIntent);
                            }
                        }

                        return true;
                    }
                });
            } else {
                FragmentManager fm = getSupportFragmentManager();
                FrameLayout ldetalji = (FrameLayout) findViewById(R.id.listPlace);

                if (ldetalji != null) {
                    ListaFrag fd = (ListaFrag) fm.findFragmentById(R.id.listPlace);
                    if (fd == null) {

                        ArrayList<Kategorija> listakat = kategorije;
                        fd = new ListaFrag();
                        Bundle arg = new Bundle();
                        arg.putSerializable("KAT2", listakat);
                        fd.setArguments(arg);
                        fm.beginTransaction().replace(R.id.listPlace, fd).commit();
                    } else {
                        fm.popBackStack(null, android.support.v4.app.FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    }
                }

                FrameLayout detalji = (FrameLayout) findViewById(R.id.detailPlace);

                if (detalji != null) {
                    DetailFrag fd = (DetailFrag) fm.findFragmentById(R.id.detailPlace);
                    if (fd == null) {
                        fd = new DetailFrag();
                        Bundle arg = new Bundle();
                        arg.putParcelableArrayList("kvizovi", kvizovi);
                        fd.setArguments(arg);
                        fm.beginTransaction().replace(R.id.detailPlace, fd).commit();
                    } else {
                        fm.popBackStack(null, android.support.v4.app.FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    }
                }
            }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent i) {

       if(isNetworkAvailable()){
           new KrerirajDokumentTask(new AsyncResponse() {
               @Override
               public void processFinish() {
                   adapterKat = new KategorijaAdapter(KvizoviAkt.this, kategorije);
                   spPostojeceKategorije.setAdapter(adapterKat);
                   adapterKv = new KvizAdapter(KvizoviAkt.this, kvizovi);
                   lvKvizovi.setAdapter(adapterKv);
                   kHelper.brisi();
                   upisivanjeKadNemaInterenta();
                   adapterKat.notifyDataSetChanged();
                   adapterKv.notifyDataSetChanged();
               }
           }).execute();
       }

        if(requestCode == 1) {
            if (resultCode == RESULT_OK) {
                Kategorija kat = (Kategorija)i.getSerializableExtra("KAT");
                //Kviz k = new Kviz(i.getStringExtra("KVIZ"), (ArrayList<Pitanje>) i.getSerializableExtra("PIT"), (Kategorija) i.getSerializableExtra("KAT"));
                if (!kategorije.contains(kat)) kategorije.add(kat);

                if(kojaJeIzabrana.equals("Svi")){
                    new KrerirajDokumentTask(new AsyncResponse() {
                        @Override
                        public void processFinish() {
                            adapterKat = new KategorijaAdapter(KvizoviAkt.this, kategorije);
                            spPostojeceKategorije.setAdapter(adapterKat);
                            adapterKv = new KvizAdapter(KvizoviAkt.this, kvizovi);
                            lvKvizovi.setAdapter(adapterKv);
                            adapterKat.notifyDataSetChanged();
                            adapterKv.notifyDataSetChanged();
                        }
                    }).execute();
                }else{
                    for (int p = 0; p < sveKategorije.size(); p++) {
                        if (sveKategorije.get(p).first.getNaziv().equals(kat.getNaziv())) {
                            index = p;
                            break;
                        }
                    }
                    Kategorija q = sveKategorije.get(index).first;
                    for (int z = 0; z < sveKategorije.size(); z++) {
                        if (q.getNaziv().equals(sveKategorije.get(z).first.getNaziv()))
                            kategorijaZaFiltriranjeBaze = sveKategorije.get(z).second;
                    }
                    new KrerirajDokumentTaskFiltriranje(new AsyncResponse() {
                        @Override
                        public void processFinish() {
                            kviz = new ArrayList<Kviz>();
                            // if(!izabranoSvi) {
                            for (Kviz q : kvizoviPremaKategoriji) kviz.add(q);
                            kvizoviPremaKategoriji.clear();
                               /* }
                                else{
                                    for(int i = 0; i < sveKategorije.size();i++){
                                        kviz.add(sviKvizovi.get(i).first);
                                    }
                                }*/
                            System.out.println("evo gagagagagga     " + kategorijaZaFiltriranjeBaze);
                            if (!kviz.contains(new Kviz("Dodaj kviz", pitanja, svi)))
                                kviz.add(new Kviz("Dodaj kviz", pitanja, svi));
                            adapterKv = new KvizAdapter(KvizoviAkt.this, kviz);
                            lvKvizovi.setAdapter(adapterKv);
                            adapterKv.notifyDataSetChanged();
                        }
                    }).execute();
                }
                //kvizovi.set(pozicija, k);
                adapterKat.notifyDataSetChanged();
                adapterKv.notifyDataSetChanged();
            }
            adapterKat.notifyDataSetChanged();
            adapterKv.notifyDataSetChanged();
        }
        if(requestCode == 0) {
            if (resultCode == RESULT_OK) {

                Kategorija kat = (Kategorija) i.getSerializableExtra("KAT");
                Kviz k = new Kviz(i.getStringExtra("KVIZ"), (ArrayList<Pitanje>) i.getSerializableExtra("PIT"), kat);
                if(!kategorije.contains(kat)) kategorije.add(kat);
                kvizovi.add(kvizovi.size() - 1, k);
                if(kojaJeIzabrana.equals("Svi")){
                    new KrerirajDokumentTask(new AsyncResponse() {
                        @Override
                        public void processFinish() {
                            adapterKat = new KategorijaAdapter(KvizoviAkt.this, kategorije);
                            spPostojeceKategorije.setAdapter(adapterKat);
                            adapterKv = new KvizAdapter(KvizoviAkt.this, kvizovi);
                            lvKvizovi.setAdapter(adapterKv);
                            adapterKat.notifyDataSetChanged();
                            adapterKv.notifyDataSetChanged();
                        }
                    }).execute();
                }
                else{
                    if (!kategorije.contains(kat)) kategorije.add(kat);
                    for (int p = 0; p < sveKategorije.size(); p++) {
                        if (sveKategorije.get(p).first.getNaziv().equals(kat.getNaziv())) {
                            index = p;
                            break;
                        }
                    }
                    Kategorija q = sveKategorije.get(index).first;
                    for (int z = 0; z < sveKategorije.size(); z++) {
                        if (q.getNaziv().equals(sveKategorije.get(z).first.getNaziv()))
                            kategorijaZaFiltriranjeBaze = sveKategorije.get(z).second;
                    }
                    new KrerirajDokumentTaskFiltriranje(new AsyncResponse() {
                        @Override
                        public void processFinish() {
                            kviz = new ArrayList<Kviz>();
                            // if(!izabranoSvi) {
                            for (Kviz q : kvizoviPremaKategoriji) kviz.add(q);
                            kvizoviPremaKategoriji.clear();
                               /* }
                                else{
                                    for(int i = 0; i < sveKategorije.size();i++){
                                        kviz.add(sviKvizovi.get(i).first);
                                    }
                                }*/
                            System.out.println("evo gagagagagga     " + kategorijaZaFiltriranjeBaze);
                            if (!kviz.contains(new Kviz("Dodaj kviz", pitanja, svi)) && !IgrajKvizAkt.vracaSeIzIgre)
                                kviz.add(new Kviz("Dodaj kviz", pitanja, svi));
                            adapterKv = new KvizAdapter(KvizoviAkt.this, kviz);
                            lvKvizovi.setAdapter(adapterKv);
                            adapterKv.notifyDataSetChanged();
                        }
                    }).execute();
                }
                adapterKat.notifyDataSetChanged();
                adapterKv.notifyDataSetChanged();

            }
            adapterKat.notifyDataSetChanged();
            adapterKv.notifyDataSetChanged();
        }

        if(requestCode == 2){
           /* if (resultCode == RESULT_OK) {

            }*/
            if (resultCode == RESULT_CANCELED) {
                //kvizovi.add(new Kviz("Dodaj kviz", pitanja, svi));
                adapterKat.notifyDataSetChanged();
                adapterKv.notifyDataSetChanged();
            }

        }
        if (resultCode == RESULT_CANCELED) {
            //Write your code if there's no result
            adapterKat.notifyDataSetChanged();
            adapterKv.notifyDataSetChanged();
        }
    }
    @Override
    public void clickNaListu(ArrayList<Kviz> listKvizova){
        Bundle arguments = new Bundle();
        arguments.putParcelableArrayList("kvizovi", listKvizova);
        DetailFrag df = new DetailFrag();
        df.setArguments(arguments);
        getSupportFragmentManager().beginTransaction().replace(R.id.detailPlace, df).commit();

    }

    public void getDataFromEventTable(View v) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_CALENDAR}, MY_CAL_REQ);
        }

        Cursor cur = null;
        ContentResolver cr = getContentResolver();

        String[] mProjection =
                {
                        "_id",
                        CalendarContract.Events.TITLE,
                        CalendarContract.Events.EVENT_LOCATION,
                        CalendarContract.Events.DTSTART,
                        CalendarContract.Events.DTEND,
                };

        Uri uri = CalendarContract.Events.CONTENT_URI;
        String selection = CalendarContract.Events.EVENT_LOCATION + " = ? ";
        String[] selectionArgs = new String[]{"London"};

        cur = cr.query(uri, mProjection, null, null, null);
        dogadaji.clear();
       // dogadaji.add(new Pair<String, String>("NISTA","SSTA"));
        while (cur.moveToNext()) {
            String title = cur.getString(cur.getColumnIndex(CalendarContract.Events.TITLE));
            String timeStart = cur.getString(cur.getColumnIndex(CalendarContract.Events.DTSTART));
            String timeEnd = cur.getString(cur.getColumnIndex(CalendarContract.Events.DTEND));
            dogadaji.add(new Pair<String, Pair<String, String>>(title,new Pair<String, String>(timeStart, timeEnd)));
        }
        cur.close();
    }
    public boolean daLiJeProslo(String krajDogadjaja){
        long kraj = Long.parseLong(krajDogadjaja);
        long sada = System.currentTimeMillis();
        if(kraj < sada) return true;
        return false;
    }
    public boolean daLiJePoceo(String pocetakDogadjaja){
        long pocetak =  Long.parseLong(pocetakDogadjaja);
        long sada = System.currentTimeMillis();
        if(pocetak < sada) return true;
        return false;
    }
    public long razlikaDoNajblizeg(ArrayList<Pair<String, Pair<String, String>>> sviDogadjaji){
        long min = Long.parseLong(sviDogadjaji.get(0).second.first);
        for(int i = 0; i < sviDogadjaji.size(); i++){
            if(!daLiJeProslo(sviDogadjaji.get(i).second.second)){
                if(!daLiJePoceo(sviDogadjaji.get(i).second.first)){
                min = Long.parseLong(sviDogadjaji.get(i).second.first);
                break;
                }
            }
            }
        for(int i = 0; i < sviDogadjaji.size(); i++){
            if(!daLiJeProslo(sviDogadjaji.get(i).second.second)){
                if(!daLiJePoceo(sviDogadjaji.get(i).second.first)){
                    if(min > Long.parseLong(sviDogadjaji.get(i).second.first)) {
                        min = Long.parseLong(sviDogadjaji.get(i).second.first);
                        System.out.println("udje li kad ovdje");
                    }
                    System.out.println(min + " sadasnje " + System.currentTimeMillis());
                    System.out.println(min - System.currentTimeMillis());
                }
            }
        }
        long rez = min - System.currentTimeMillis();
        return rez;
    }
    public int minuteDoEventa(long milisekundeDoEventa){
        int r = (int) TimeUnit.MILLISECONDS.toMinutes(milisekundeDoEventa);
        return r;
    }
    public int izracunajX(int x, int y){
        double rez = (double)x/y;
        if((((double)x/y)/0.5)%2 == 1) rez = (double)x/y + 0.5;
        else rez = (double)x/y;
        return (int)rez;
    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    private void upisivanjeKadNemaInterenta(){
        //kHelper.brisi();
        for(int i = 0; i < sveKategorije.size(); i++){
            kHelper.upisiKategorijuUBazu(sveKategorije.get(i).first);
        }
        for(int i = 0; i < svaPitanja.size(); i++){
            kHelper.upisiPitanjeUBazu(svaPitanja.get(i).first);
        }
       for(int i = 0; i < sviKvizovi.size(); i++){
            kHelper.upisiKvizUBazu(sviKvizovi.get(i).first);
        }
        for(int i = 0; i < sveRangListePrviPut.size(); i++){
            kHelper.upisiRangUBazu(sveRangListePrviPut.get(i).first,sveRangListePrviPut.get(i).second.second.first,sveRangListePrviPut.get(i).second.second.second,String.valueOf(sveRangListePrviPut.get(i).second.first));
        }
    }
}
