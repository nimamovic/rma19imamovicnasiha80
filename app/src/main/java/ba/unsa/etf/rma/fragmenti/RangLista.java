package ba.unsa.etf.rma.fragmenti;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.util.Pair;
import android.view.LayoutInflater;
import android.widget.ListView;
import android.widget.TextView;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.common.collect.Lists;

import org.apache.commons.codec.StringEncoderComparator;
import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Collections;
import java.util.Comparator;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.aktivnosti.KvizoviAkt;
import ba.unsa.etf.rma.klase.AsyncResponse;

import static android.support.v4.content.ContextCompat.getSystemService;

public class RangLista extends Fragment {
    private ListView rangLista;

    private String nazivK;
    private String ime;
    private int redniBrojPozcije = 0;
    private String procenat;



    private Map<Integer, Map<String, String>> pom2 = new HashMap<>();


    private ArrayAdapter<String> adapterRang;
    public static ArrayList<String> listaRangLista = new ArrayList<>();

    private ArrayList<Pair<String, Map<Integer, Map<String, String>>>> sveRangListe = new ArrayList<>();
    private ArrayList<Map<Integer, Map<String, String>>> mapaListaGlavna = new ArrayList<>();

    public class KrerirajDokumentRang extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... strings) {

            try {
                InputStream is = getResources().openRawResource(R.raw.secret);
                GoogleCredential credentials = GoogleCredential.fromStream(is).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                credentials.refreshToken();
                String TOKEN = credentials.getAccessToken();
                System.out.println("SADA JE TOKEN: " + TOKEN);
                String url = "https://firestore.googleapis.com/v1/projects/projekat-20b5a/databases/(default)/documents/Rangliste?access_token=";
                URL urlObj = new URL(url + URLEncoder.encode(TOKEN, "UTF-8"));
                HttpURLConnection connection = (HttpURLConnection) urlObj.openConnection();
                connection.setDoOutput(true);
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setRequestProperty("Accept", "application/json");

                String dokument = "{\"fields\" : { \"nazivKviza\" : { \"stringValue\" : \"" + nazivK + "\" }, \"lista\" : { \"mapValue\" : { \"fields\" : { \"pozicija\" : { \"integerValue\" : \"" + redniBrojPozcije
                        + "\" }, \"mapa\" : { \"mapValue\" : { \"fields\" : { \"procenat\" : { \"stringValue\" : \"" + procenat + "\" }, \"imeIgraca\" : { \"stringValue\" : \""
                        + ime + "\" } } } } } } } } }";
                try (OutputStream os = connection.getOutputStream()) {
                    byte[] input = dokument.getBytes("utf-8");
                    os.write(input, 0, input.length);
                }

                int code = connection.getResponseCode();
                InputStream odgovor = connection.getInputStream();
                StringBuilder response = null;
                try (BufferedReader br = new BufferedReader(
                        new InputStreamReader(odgovor, "utf-8"))
                ) {
                    response = new StringBuilder();
                    String rensponseLine = null;
                    while ((rensponseLine = br.readLine()) != null) {
                        response.append(rensponseLine.trim());
                    }
                    Log.d("ODGOVOR", response.toString());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public class KreirajDokumentRangUzimanje extends AsyncTask<String, Void, Void> {

        AsyncResponse ar = null;

        public KreirajDokumentRangUzimanje(AsyncResponse asyncResponse) {
            this.ar = asyncResponse;
        }

        @Override
        protected Void doInBackground(String... strings) {
            InputStream is = getResources().openRawResource(R.raw.secret);
            try {
                sveRangListe.clear();
                GoogleCredential credentials = GoogleCredential.fromStream(is).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                credentials.refreshToken();
                String TOKEN = credentials.getAccessToken();
                System.out.println("SADA JE TOKEN: " + TOKEN);

                String urlPitanja = "https://firestore.googleapis.com/v1/projects/projekat-20b5a/databases/(default)/documents/Rangliste?access_token=";
                URL urlObjPitanja = new URL(urlPitanja + URLEncoder.encode(TOKEN, "UTF-8"));
                HttpURLConnection connection = (HttpURLConnection) urlObjPitanja.openConnection();
                connection.connect();
                InputStream odgovorPitanja = connection.getInputStream();
                StringBuilder responsePitanja = null;
                try (BufferedReader br = new BufferedReader(
                        new InputStreamReader(odgovorPitanja, "utf-8"))
                ) {
                    responsePitanja = new StringBuilder();
                    String rensponseLine = null;
                    while ((rensponseLine = br.readLine()) != null) {
                        responsePitanja.append(rensponseLine.trim());
                    }
                    Log.d("ODGOVOR: ", responsePitanja.toString());
                }
                String rezultat = responsePitanja.toString();
                if (!rezultat.equals("{}")) {
                    JSONObject jo = new JSONObject(rezultat);
                    JSONArray documents = jo.getJSONArray("documents");
                    for (int i = 0; i < documents.length(); i++) {
                        JSONObject doc = documents.getJSONObject(i);
                        String name = doc.getString("name");
                        JSONObject fields = doc.getJSONObject("fields");
                        JSONObject stringValue = fields.getJSONObject("nazivKviza");
                        String naziv = stringValue.getString("stringValue");

                        JSONObject map = fields.getJSONObject("lista");
                        JSONObject mapValue = map.getJSONObject("mapValue");

                        JSONObject fieldsM = mapValue.getJSONObject("fields");
                        JSONObject integerValue = fieldsM.getJSONObject("pozicija");
                        int rangiran = integerValue.getInt("integerValue");
                        JSONObject map2 = fieldsM.getJSONObject("mapa");
                        JSONObject mapValue2 = map2.getJSONObject("mapValue");
                        JSONObject fieldsM2 = mapValue2.getJSONObject("fields");
                        JSONObject stringValue2 = fieldsM2.getJSONObject("imeIgraca");
                        String ime = stringValue2.getString("stringValue");
                        JSONObject integerValueProcenat = fieldsM2.getJSONObject("procenat");
                        String procenat = integerValueProcenat.getString("stringValue");

                        Map<String, String> mapaPom = new HashMap<>();
                        mapaPom.put(ime, procenat);
                        Map<Integer, Map<String, String>> mapaPrava = new HashMap<>();
                        mapaPrava.put(rangiran, mapaPom);
                        sveRangListe.add(new Pair(naziv, mapaPrava));
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            ar.processFinish();
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_rang_lista, container, false);

        rangLista = (ListView) view.findViewById(R.id.rangLista);
        nazivK = getArguments().getString("nazivK");
        ime = getArguments().getString("ime");
        procenat = getArguments().getString("procenat");
        Map<String, String> pom = new HashMap<>();
        pom.put(ime, procenat);
        pom2.put(redniBrojPozcije, pom);
        listaRangLista.clear();
        mapaListaGlavna.clear();

        if (!isNetworkAvailable()) {
            ArrayList<Pair<String, Pair<Integer, Pair<String, String>>>> rangic = KvizoviAkt.kHelper.citajRangIzBaze();
            sveRangListe.clear();
            for (int i = 0; i < rangic.size(); i++) {
                Map<String, String> mapaPom = new HashMap<>();
                mapaPom.put(rangic.get(i).second.second.first, rangic.get(i).second.second.second);
                Map<Integer, Map<String, String>> mapaPrava = new HashMap<>();
                mapaPrava.put(rangic.get(i).second.first, mapaPom);
                sveRangListe.add(new Pair(rangic.get(i).first, mapaPrava));
            }
        } else {
            new KreirajDokumentRangUzimanje(new AsyncResponse() {
                @Override
                public void processFinish() {
                    for (int i = 0; i < sveRangListe.size(); i++) {
                        if (nazivK.equals(sveRangListe.get(i).first))
                            mapaListaGlavna.add(sveRangListe.get(i).second);
                    }
                   //mapaListaGlavna.add(pom2);
                }
            }).execute();
        }
        if(!isNetworkAvailable()) {
            KvizoviAkt.kHelper.upisiRangUBazu(nazivK,ime,procenat, "1");
            Map<String, String> mapaPom = new HashMap<>();
            mapaPom.put(ime,procenat);
            Map<Integer, Map<String, String>> mapaPrava = new HashMap<>();
            mapaPrava.put(1, mapaPom);
            sveRangListe.add(new Pair(nazivK, mapaPrava));
            KvizoviAkt.rangListeBezInterneta.add(new Pair<String, Pair<Integer, Pair<String, String>>>(nazivK, new Pair<Integer, Pair<String, String>>(1, new Pair<String, String>(ime,procenat))));
        }
        else{
            new KrerirajDokumentRang().execute();
        }

        adapterRang = new ArrayAdapter<String>(getActivity(), R.layout.rang_lista, R.id.ime, listaRangLista) {
            @Override
            public View getView(int position, @Nullable View convertView, @Nullable ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TextView ime = (TextView) view.findViewById(R.id.ime);
                ime.setText(listaRangLista.get(position));
                return view;
            }
        };
        rangLista.setAdapter(adapterRang);
        if(isNetworkAvailable()) {
            new KreirajDokumentRangUzimanje(new AsyncResponse() {
                @Override
                public void processFinish() {
                    listaRangLista.clear();
                    mapaListaGlavna.clear();
                    for (int i = 0; i < sveRangListe.size(); i++) {
                        if (nazivK.equals(sveRangListe.get(i).first))
                            mapaListaGlavna.add(sveRangListe.get(i).second);
                    }
                    //mapaListaGlavna.add(pom2);
                    System.out.println("broj elemenata " + mapaListaGlavna.size());
                    Collections.sort(mapaListaGlavna, new Comparator<Map<Integer, Map<String, String>>>() {
                        public int compare(
                                Map<Integer, Map<String, String>> entry1, Map<Integer, Map<String, String>> entry2) {
                            Iterator iterator1 = entry1.entrySet().iterator();
                            Iterator iterator2 = entry2.entrySet().iterator();
                            String procenat1 = "";
                            String procenat2 = "";
                            while (iterator1.hasNext()) {
                                Map.Entry mapaEntry1 = (Map.Entry) iterator1.next();
                                Map<String, String> mapaDio1 = (Map<String, String>) mapaEntry1.getValue();
                                Iterator iterator1dio = mapaDio1.entrySet().iterator();
                                while (iterator1dio.hasNext()) {
                                    Map.Entry mapaEntryDio1 = (Map.Entry) iterator1dio.next();
                                    procenat1 = String.valueOf(mapaEntryDio1.getValue());
                                }
                            }
                            while (iterator2.hasNext()) {
                                Map.Entry mapaEntry2 = (Map.Entry) iterator2.next();
                                Map<String, String> mapaDio2 = (Map<String, String>) mapaEntry2.getValue();
                                Iterator iterator2dio = mapaDio2.entrySet().iterator();
                                while (iterator2dio.hasNext()) {
                                    Map.Entry mapaEntryDio1 = (Map.Entry) iterator2dio.next();
                                    procenat2 = String.valueOf(mapaEntryDio1.getValue());
                                }
                            }
                            if (Double.parseDouble(procenat1) < Double.parseDouble(procenat2))
                                return 1;
                            if (Double.parseDouble(procenat2) < Double.parseDouble(procenat1))
                                return -1;
                            return 0;

                        }
                    });
                    for (int i = 0; i < mapaListaGlavna.size(); i++) {
                        Iterator it = mapaListaGlavna.get(i).entrySet().iterator();
                        while (it.hasNext()) {
                            Map.Entry m = (Map.Entry) it.next();
                            Map<String, String> m2 = (Map<String, String>) m.getValue();
                            Iterator it2 = m2.entrySet().iterator();
                            while (it2.hasNext()) {
                                Map.Entry pomMap = (Map.Entry) it2.next();
                                String ime2 = (String) pomMap.getKey();
                                String procenat = (String) pomMap.getValue();
                                String unos = String.valueOf(i + 1) + ".       " + ime2 + "      " + procenat + "%";
                                listaRangLista.add(unos);
                            }
                        }
                    }
                    adapterRang.notifyDataSetChanged();
                }
            }).execute();
        }
        else{

            listaRangLista.clear();
            mapaListaGlavna.clear();
            for (int i = 0; i < sveRangListe.size(); i++) {
                if (nazivK.equals(sveRangListe.get(i).first))
                    mapaListaGlavna.add(sveRangListe.get(i).second);
            }
            //mapaListaGlavna.add(pom2);
            //System.out.println("broj elemenata " + mapaListaGlavna.size());
            Collections.sort(mapaListaGlavna, new Comparator<Map<Integer, Map<String, String>>>() {
                public int compare(
                        Map<Integer, Map<String, String>> entry1, Map<Integer, Map<String, String>> entry2) {
                    Iterator iterator1 = entry1.entrySet().iterator();
                    Iterator iterator2 = entry2.entrySet().iterator();
                    String procenat1 = "";
                    String procenat2 = "";
                    while (iterator1.hasNext()) {
                        Map.Entry mapaEntry1 = (Map.Entry) iterator1.next();
                        Map<String, String> mapaDio1 = (Map<String, String>) mapaEntry1.getValue();
                        Iterator iterator1dio = mapaDio1.entrySet().iterator();
                        while (iterator1dio.hasNext()) {
                            Map.Entry mapaEntryDio1 = (Map.Entry) iterator1dio.next();
                            procenat1 = String.valueOf(mapaEntryDio1.getValue());
                        }
                    }
                    while (iterator2.hasNext()) {
                        Map.Entry mapaEntry2 = (Map.Entry) iterator2.next();
                        Map<String, String> mapaDio2 = (Map<String, String>) mapaEntry2.getValue();
                        Iterator iterator2dio = mapaDio2.entrySet().iterator();
                        while (iterator2dio.hasNext()) {
                            Map.Entry mapaEntryDio1 = (Map.Entry) iterator2dio.next();
                            procenat2 = String.valueOf(mapaEntryDio1.getValue());
                        }
                    }
                    if (Double.parseDouble(procenat1) < Double.parseDouble(procenat2))
                        return 1;
                    if (Double.parseDouble(procenat2) < Double.parseDouble(procenat1))
                        return -1;
                    return 0;

                }
            });
            for (int i = 0; i < mapaListaGlavna.size(); i++) {
                Iterator it = mapaListaGlavna.get(i).entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry m = (Map.Entry) it.next();
                    Map<String, String> m2 = (Map<String, String>) m.getValue();
                    Iterator it2 = m2.entrySet().iterator();
                    while (it2.hasNext()) {
                        Map.Entry pomMap = (Map.Entry) it2.next();
                        String ime2 = (String) pomMap.getKey();
                        String procenat = (String) pomMap.getValue();
                        String unos = String.valueOf(i + 1) + ".       " + ime2 + "      " + procenat + "%";
                        listaRangLista.add(unos);
                    }
                }
            }
            adapterRang.notifyDataSetChanged();
        }
        return view;
    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connMgr = (ConnectivityManager) getActivity()
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            return  true;
        } else {
            return false;
        }
    }
}
