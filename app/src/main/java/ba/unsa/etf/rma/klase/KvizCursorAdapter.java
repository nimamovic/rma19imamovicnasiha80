package ba.unsa.etf.rma.klase;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.widget.ResourceCursorAdapter;
import android.widget.TextView;

import com.maltaisn.icondialog.IconView;

import ba.unsa.etf.rma.R;

import static ba.unsa.etf.rma.klase.KvizoviDBOpenHelper.KVIZ_NAZIV;

public class KvizCursorAdapter extends ResourceCursorAdapter {
    public KvizCursorAdapter(Context context, int layout, Cursor c, int flags) {
        super(context, layout, c, flags);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView ime = (TextView) view.findViewById(R.id.naziv);
        ime.setText(cursor.getString(cursor.getColumnIndex(KVIZ_NAZIV)));


        IconView image = (IconView) view.findViewById(R.id.slika);
        //NE IDE OVAKO TREBA UZETI ID IKONE KATEGORIJE PA TO TREBA SKONTATI KAKO
       // image.setIcon(Integer.parseInt(currentKviz.getKategorija().getId()));
    }
}
