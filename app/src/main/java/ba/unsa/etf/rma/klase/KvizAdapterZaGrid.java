package ba.unsa.etf.rma.klase;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.maltaisn.icondialog.IconView;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

import ba.unsa.etf.rma.R;

public class KvizAdapterZaGrid extends ArrayAdapter<Kviz> {
    int pozicija;

    private Context context;

    private List<Kviz> kvizList = new ArrayList<Kviz>();

    public KvizAdapterZaGrid(@NonNull Context context, @Nullable ArrayList<Kviz> list) {
        super(context, 0 , list);
        this.context = context;
        kvizList = list;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if(listItem == null)
            listItem = LayoutInflater.from(context).inflate(R.layout.lista_grid,parent,false);

        pozicija = position;

        Kviz currentKviz = kvizList.get(position);
        int ukupno = currentKviz.getPitanja().size();
        ukupno--;

        IconView image = (IconView) listItem.findViewById(R.id.slika);

        if(position != kvizList.size()-1){
            TextView name = (TextView) listItem.findViewById(R.id.nazivKviza);
            name.setText(currentKviz.getNaziv());

            TextView broj =  (TextView) listItem.findViewById(R.id.ukupanBrojPit);
            broj.setText(String.valueOf(ukupno));

            image.setIcon(Integer.parseInt(currentKviz.getKategorija().getId()));
        }
        else{
            TextView name = (TextView) listItem.findViewById(R.id.nazivKviza);
            name.setText(currentKviz.getNaziv());

            TextView broj =  (TextView) listItem.findViewById(R.id.ukupanBrojPit);
            broj.setText("");

            image.setImageResource(R.drawable.plus);
        }




        return listItem;
    }
}
