package ba.unsa.etf.rma.fragmenti;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.aktivnosti.KvizoviAkt;

public class PitanjeFrag  extends Fragment {
    private TextView tekstPitanja;
    private ListView odgovoriPitanja;

    private boolean tacanIzabran = false;
    private int index = -1;

    private ArrayList<String> odgovori = new ArrayList<>();
    private ArrayAdapter<String> adapterOdg;


    public interface IgranjeKviza{
        void tacan();
        void preostalo();
        void pitanje();
    }

    private IgranjeKviza igra;


    private String p;
    private String t;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_pitanje,container,false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        tekstPitanja = (TextView) getView().findViewById(R.id.tekstPitanja);
        odgovoriPitanja = (ListView) getView().findViewById(R.id.odgovoriPitanja);

        try {
            igra = (IgranjeKviza) getActivity();
        }catch(ClassCastException e) {
            throw new ClassCastException(getActivity().toString());
        }

        if(getArguments() != null){
            p = getArguments().getString("pitanjaF");
            odgovori = getArguments().getStringArrayList("odgovoriF");
            t = getArguments().getString("tacanF");

            tekstPitanja.setText(p);

            adapterOdg = new ArrayAdapter<String>(getActivity(), R.layout.lista, R.id.naziv, odgovori) {
                @Override
                public View getView(int position, @Nullable View convertView, @Nullable ViewGroup parent) {
                    View view = super.getView(position, convertView, parent);
                    TextView tekst = (TextView) view.findViewById(R.id.naziv);

                    String s = odgovori.get(position);
                    tekst.setText(s);
                    tekst.setBackgroundColor(Color.WHITE);
                    if(position == index) {
                        if (tacanIzabran) {
                            tekst.setBackgroundColor(getResources().getColor(R.color.zelena));
                        } else{
                            tekst.setBackgroundColor(getResources().getColor(R.color.crvena));
                        }
                    }
                    return view;
                }
            };
            odgovoriPitanja.setAdapter(adapterOdg);
        }

        odgovoriPitanja.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                index = position;
                if(t.equals(odgovori.get(index))){
                    tacanIzabran = true;
                    igra.tacan();
                    adapterOdg.notifyDataSetChanged();
                }
                else {
                    igra.preostalo();
                    tacanIzabran = false;
                    adapterOdg.notifyDataSetChanged();
                }
                odgovoriPitanja.setEnabled(false);
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        igra.pitanje();
                    }
                }, 2000);
            }
        });
    }
}
