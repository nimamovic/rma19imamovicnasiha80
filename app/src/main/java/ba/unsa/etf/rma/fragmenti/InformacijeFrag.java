package ba.unsa.etf.rma.fragmenti;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.aktivnosti.KvizoviAkt;


public class InformacijeFrag extends Fragment {

    private Button btnKraj;
    private TextView infNazivKviza;
    private TextView infBrojTacnihPitanja;
    private TextView infBrojPreostalihPitanja;
    private TextView infProcenatTacni;

    private String naziv;

    private String t;

    private String o;
    private String p;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_informacije, container, false);
        if (getArguments() != null) {

            infNazivKviza = (TextView) v.findViewById(R.id.infNazivKviza);
            infBrojTacnihPitanja = (TextView) v.findViewById(R.id.infBrojTacnihPitanja);
            infBrojPreostalihPitanja = (TextView) v.findViewById(R.id.infBrojPreostalihPitanja);
            btnKraj = (Button) v.findViewById(R.id.btnKraj);
            infProcenatTacni = (TextView) v.findViewById(R.id.infProcenatTacni);

            naziv = getArguments().getString("naziv");
            infNazivKviza.setText(naziv);

            t = getArguments().getString("brTacnih");
            infBrojTacnihPitanja.setText(t);

            o = getArguments().getString("brNetacno");
            infBrojPreostalihPitanja.setText(o);

            p = getArguments().getString("procenat");
            infProcenatTacni.setText(p);

            btnKraj.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    updateDetail();
                } });

        }

        return v;
    }
    public void updateDetail() {
        Intent intent = new Intent(getActivity(), KvizoviAkt.class);
        startActivity(intent);
    }


}
