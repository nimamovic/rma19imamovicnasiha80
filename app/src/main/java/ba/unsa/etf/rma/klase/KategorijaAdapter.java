package ba.unsa.etf.rma.klase;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ba.unsa.etf.rma.R;

public class KategorijaAdapter extends ArrayAdapter<Kategorija> implements SpinnerAdapter {
    private Context context;
    private List<Kategorija> kategorijaList = new ArrayList<>();
    private static LayoutInflater inflater = null;
    private Resources res;


    public KategorijaAdapter(Context context, List<Kategorija> objects) {

        super(context, 0, objects);
        this.context = context;
        kategorijaList = objects;
    }

    public void notifyDataSetChanged() {
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {

        View view;
        view =  View.inflate(context, R.layout.lista_spinera, null);

        Kategorija currentKategorija = kategorijaList.get(position);

        final TextView textView = (TextView) view.findViewById(R.id.naziv);
        textView.setText(currentKategorija.getNaziv());

        return view;
    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view =  View.inflate(context, R.layout.lista_spinera, null);

        Kategorija currentKategorija = kategorijaList.get(position);

        TextView textView = (TextView) view.findViewById(R.id.naziv);
        textView.setText(currentKategorija.getNaziv());

        return textView;
    }

}
