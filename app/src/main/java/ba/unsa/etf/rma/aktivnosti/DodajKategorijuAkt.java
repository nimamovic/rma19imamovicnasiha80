package ba.unsa.etf.rma.aktivnosti;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.common.collect.Lists;
import com.maltaisn.icondialog.Icon;
import com.maltaisn.icondialog.IconDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.klase.Kategorija;

public class DodajKategorijuAkt extends AppCompatActivity implements IconDialog.Callback{

    private EditText etNaziv;
    private EditText etIkona;
    private Button btnDodajIkonu;
    private Button btnDodajKategoriju;

    public static ArrayList<Pair<Kategorija,String>> kategorijeIzBaze = new ArrayList<Pair<Kategorija,String>>(); //kategorije koje ce ici u bazu ustvari

    private Icon[] selectedIcons;

    IconDialog iconDialog = new IconDialog();

    private boolean imaLiVecKat = false;

    private class KrerirajDokumentTaskZaKategorije extends AsyncTask<String,Void,Void> {
        @Override
        protected Void doInBackground(String... strings){

            GoogleCredential credentials;
            try {

                InputStream is = getResources().openRawResource(R.raw.secret);
                credentials = GoogleCredential.fromStream(is).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                credentials.refreshToken();
                String TOKEN = credentials.getAccessToken();

                Log.d("TOKEN EVO GA",TOKEN);

                String url = "https://firestore.googleapis.com/v1/projects/projekat-20b5a/databases/(default)/documents/Kategorije?access_token=";
                URL urlObj = new URL (url + URLEncoder.encode(TOKEN, "UTF-8"));
                HttpURLConnection connection = (HttpURLConnection) urlObj.openConnection();
                connection.setDoOutput(true);
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setRequestProperty("Accept", "application/json");


                String dokument = "{ \"fields\": { \"naziv\": {\"stringValue\":\"" + etNaziv.getText().toString()+"\"}, \"idIkonice\": {\"integerValue\":\"" + Integer.parseInt(etIkona.getText().toString()) +"\"}}}";

                try (OutputStream os = connection.getOutputStream())
                {
                    byte[] input = dokument.getBytes("utf-8");
                    os.write(input, 0, input.length);
                }

                int code = connection.getResponseCode();
                InputStream odgovor = connection.getInputStream();
                try(BufferedReader br = new BufferedReader (
                        new InputStreamReader(odgovor, "utf-8"))
                ){
                    StringBuilder response = new StringBuilder();
                    String rensponseLine = null;
                    while((rensponseLine = br.readLine()) != null){
                        response.append(rensponseLine.trim());
                    }
                    String katy = response.toString();
                    JSONObject jo = new JSONObject(katy);
                    JSONObject fields = jo.getJSONObject("fields");
                    JSONObject jsonNaziva = fields.getJSONObject("naziv");
                    String naziv = jsonNaziva.getString("stringValue");
                    JSONObject jsonIkone = fields.getJSONObject("idIkonice");
                    int idIkonice = jsonIkone.getInt(("integerValue"));
                    String ik = String.valueOf(idIkonice);
                    String id = jo.getString("name");
                    int duzina = id.length();
                    String ime = "";
                    int brojac = 0;
                    for(int j = 0; j < id.length(); j++){
                        if(id.charAt(j) == '/' && brojac!=6){
                            brojac++;
                        }
                        else if(brojac == 6){
                            ime = id.substring(j, duzina);
                            break;
                        }
                    }

                    Kategorija pomK = new Kategorija(naziv, ik);
                    Pair<Kategorija, String> pomP = new Pair<>(pomK, ime);
                    kategorijeIzBaze.add(pomP);
                    KvizoviAkt.sveKategorije.add(KvizoviAkt.sveKategorije.size(),pomP);

                    Log.d("ODGOVOR", response.toString());
                }
            }
            catch(JSONException e){
                e.printStackTrace();
            }
            catch (IOException e){
                e.printStackTrace();
            }
            return null;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dodaj_kategoriju_akt);

        etNaziv = (EditText) findViewById(R.id.etNaziv);
        etIkona = (EditText) findViewById(R.id.etIkona);
        btnDodajIkonu = (Button) findViewById(R.id.btnDodajIkonu);
        btnDodajKategoriju = (Button) findViewById(R.id.btnDodajKategoriju);


        btnDodajIkonu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iconDialog.setSelectedIcons(selectedIcons);
                iconDialog.show(getSupportFragmentManager(), "icon_dialog");
            }
        });

        btnDodajKategoriju.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(isNetworkAvailable()) {
                    imaLiVecKat = false;
                    for (Kategorija p : KvizoviAkt.kategorije)
                        if (p.getNaziv().equals(etNaziv.getText().toString()))
                            imaLiVecKat = true;
                    if (etNaziv.getText().toString().length() == 0 && etIkona.getText().toString().length() == 0) {
                        etNaziv.setBackgroundColor(Color.RED);
                        etIkona.setBackgroundColor(Color.RED);
                    } else if (etNaziv.getText().toString().length() == 0) {
                        etNaziv.setBackgroundColor(Color.RED);
                    } else if (etIkona.getText().toString().length() == 0) {
                        etIkona.setBackgroundColor(Color.RED);
                    } else if (imaLiVecKat) {
                        etNaziv.setBackgroundColor(Color.RED);
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(DodajKategorijuAkt.this);
                        alertDialog.setTitle("Unesena kategorija već postoji!");
                        alertDialog.setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        Toast.makeText(getApplicationContext(), "You clicked on OK", Toast.LENGTH_SHORT).show();
                                    }
                                });
                        alertDialog.show();
                    } else {
                        Kategorija kategorija = new Kategorija(etNaziv.getText().toString(), etIkona.getText().toString());
                        new KrerirajDokumentTaskZaKategorije().execute();
                        Intent returnIntent = getIntent();
                        returnIntent.putExtra("KATEGORIJA", kategorija.getNaziv());
                        returnIntent.putExtra("id", kategorija.getId());
                        setResult(RESULT_OK, returnIntent);
                        finish();
                    }
                    imaLiVecKat = false;
                }
            } });

    }

    @Override
    public void onIconDialogIconsSelected(Icon[] icons) {
        selectedIcons = icons;
        etIkona.setText(String.valueOf(selectedIcons[0].getId()));
    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}
