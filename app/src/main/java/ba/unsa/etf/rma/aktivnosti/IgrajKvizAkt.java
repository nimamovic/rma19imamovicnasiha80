package ba.unsa.etf.rma.aktivnosti;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.AlarmClock;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Pair;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.fragmenti.InformacijeFrag;
import ba.unsa.etf.rma.fragmenti.PitanjeFrag;
import ba.unsa.etf.rma.fragmenti.RangLista;
import ba.unsa.etf.rma.klase.Kategorija;
import ba.unsa.etf.rma.klase.Kviz;
import ba.unsa.etf.rma.klase.Pitanje;

public class IgrajKvizAkt extends AppCompatActivity implements PitanjeFrag.IgranjeKviza{

    private Kviz k;
    private ArrayList<Pitanje> pitanja = new ArrayList<Pitanje>();
    private int brojPitanja  = 0;

    int brojPitanjaNeodg ;
    int brojPitanjaTacnoOdg = 0;
    int brojPreostalih = 0;
    int brojOdgovaranja = 0;
    private double pr;

    private int pom = 0;
    private Pitanje p;

    static boolean vracaSeIzIgre = false;
    static boolean zavrseno = false;

    private ArrayList<String> prazno = new ArrayList<String>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_igraj_kviz_akt);
        if (getIntent().getExtras() != null) {
            k = new Kviz(getIntent().getStringExtra("naziv"), (ArrayList<Pitanje>) getIntent().getSerializableExtra("listapitanja"), (Kategorija) getIntent().getSerializableExtra("kategorija"));
            pitanja = (ArrayList<Pitanje>) getIntent().getSerializableExtra("listapitanja");
        }
        int x = izracunajX(pitanja.size(),2);
        int h = satiUDanu();
        if(pitanja.size() != 0) {
            System.out.println("BROJ PITANJA JEEE   " + pitanja.size() + "X JEEEEEEE  " + x);
            if (minuteUDanu() + x > 60) {
                h++;
                x = minuteUDanu() + x - 60;
            } else x += minuteUDanu();
            if (h == 24) h = 0;
            createAlarm("Alarm", h, x);
        }
        //if(pitanja.size() != 0)     pitanja.remove(pitanja.size() - 1);
        k.setPitanja(pitanja);
        vracaSeIzIgre = true;
        brojPitanja = pitanja.size();
        brojPreostalih = pitanja.size();

        Collections.shuffle(k.getPitanja());
        if(k.getPitanja().size() != 0) p = k.getPitanja().get(0);
        else p = new Pitanje();

        boolean siriL = false;
        if(k.getPitanja().size() == 0){
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(IgrajKvizAkt.this);
            alertDialog.setTitle(" ");
            alertDialog.setMessage("Unesite ime: ");
            final EditText unos = new EditText(IgrajKvizAkt.this);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT);
            unos.setLayoutParams(lp);
            alertDialog.setView(unos);
            alertDialog.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Bundle arg = new Bundle();
                            RangLista df = new RangLista();
                            arg.putString("nazivK", k.getNaziv());
                            arg.putString("ime", unos.getText().toString());
                            arg.putString("procenat", String.valueOf(pr));
                            df.setArguments(arg);
                            getSupportFragmentManager().beginTransaction().replace(R.id.pitanjePlace, df).commit();
                            Toast.makeText(getApplicationContext(), "You clicked on OK", Toast.LENGTH_SHORT).show();
                        }
                    });
            alertDialog.show();
        }
        else{
            vracaSeIzIgre = true;
            FragmentManager fm = getSupportFragmentManager();
            FrameLayout ldetalji = (FrameLayout) findViewById(R.id.informacijePlace);
            if (ldetalji != null) {
                siriL = true;
                InformacijeFrag fd = (InformacijeFrag) fm.findFragmentById(R.id.informacijePlace);
                if (fd == null) {
                    fd = new InformacijeFrag();
                    Bundle arg = new Bundle();
                    arg.putString("naziv", k.getNaziv());
                    arg.putString("brTacnih", String.valueOf(brojPitanjaTacnoOdg));
                    if (brojPitanja - 1 == -1) arg.putString("brNetacno", String.valueOf(0));
                    else arg.putString("brNetacno", String.valueOf(brojPitanja - 1));
                    arg.putString("procenat", String.valueOf(pr));
                    fd.setArguments(arg);
                    fm.beginTransaction().replace(R.id.informacijePlace, fd).commit();
                } else {
                    fm.popBackStack(null, android.support.v4.app.FragmentManager.POP_BACK_STACK_INCLUSIVE);
                }
            }

            FrameLayout l2 = (FrameLayout) findViewById(R.id.pitanjePlace);

            if (l2 != null) {
                PitanjeFrag fd = (PitanjeFrag) fm.findFragmentById(R.id.pitanjePlace);
                if (fd == null) {
                    fd = new PitanjeFrag();
                    Bundle arg = new Bundle();
                    arg.putString("pitanjaF", p.getNaziv());
                    arg.putStringArrayList("odgovoriF", p.getOdgovori());
                    arg.putString("tacanF", p.getTacan());
                    fd.setArguments(arg);
                    fm.beginTransaction().replace(R.id.pitanjePlace, fd).commit();
                } else {
                    fm.popBackStack(null, android.support.v4.app.FragmentManager.POP_BACK_STACK_INCLUSIVE);
                }
            }
       /* if(getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
        }
        else {
            super.onBackPressed();
        }*/
        }
    }
    @Override
    public void tacan() {
        brojPitanjaTacnoOdg++;
        brojOdgovaranja++;
        pr = (double) brojPitanjaTacnoOdg/(brojOdgovaranja);
        brojPreostalih--;
        Bundle arg = new Bundle();
            arg.putString("naziv", k.getNaziv());
            arg.putString("brTacnih", String.valueOf(brojPitanjaTacnoOdg));
        if(brojOdgovaranja >= brojPitanja)  arg.putString("brNetacno", String.valueOf(brojPreostalih));
          else   arg.putString("brNetacno", String.valueOf(brojPreostalih - 1));
            arg.putString("procenat", String.valueOf(pr));
        InformacijeFrag frt = new InformacijeFrag();
        frt.setArguments(arg);
        getSupportFragmentManager().beginTransaction().replace(R.id.informacijePlace,frt).commit();
    }

    @Override
    public void preostalo() {
        brojPreostalih--;
        brojOdgovaranja++;
        pr = (double) brojPitanjaTacnoOdg/(brojOdgovaranja);
        Bundle arg = new Bundle();
            arg.putString("naziv", k.getNaziv());
            arg.putString("brTacnih", String.valueOf(brojPitanjaTacnoOdg));
            if(brojOdgovaranja >= brojPitanja)  arg.putString("brNetacno", String.valueOf(brojPreostalih));
              else arg.putString("brNetacno", String.valueOf(brojPreostalih - 1));
            arg.putString("procenat", String.valueOf(pr));
        InformacijeFrag frt = new InformacijeFrag();
        frt.setArguments(arg);
        getSupportFragmentManager().beginTransaction().replace(R.id.informacijePlace,frt).commit();
    }

    @Override
    public void pitanje() {
        Pitanje pi;
        if(brojOdgovaranja == brojPitanja) {
            pi = new Pitanje("Kviz je završen!", "", prazno, "");
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(IgrajKvizAkt.this);
            alertDialog.setTitle(" ");
            alertDialog.setMessage("Unesite ime: ");
            final EditText unos = new EditText(IgrajKvizAkt.this);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT);
            unos.setLayoutParams(lp);
            alertDialog.setView(unos);
            alertDialog.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Bundle arg = new Bundle();
                            RangLista df = new RangLista();
                            arg.putString("nazivK", k.getNaziv());
                            arg.putString("ime", unos.getText().toString());
                            arg.putString("procenat", String.valueOf(pr));
                            df.setArguments(arg);
                            getSupportFragmentManager().beginTransaction().replace(R.id.pitanjePlace, df).commit();
                            Toast.makeText(getApplicationContext(), "You clicked on OK", Toast.LENGTH_SHORT).show();
                        }
                    });
            alertDialog.show();
        }
        else{
            pi = k.getPitanja().get(brojPitanja - 1 - pom);
            pom++;
        }

        Bundle arg = new Bundle();
        arg.putString("pitanjaF", pi.getNaziv());
        arg.putStringArrayList("odgovoriF", pi.getOdgovori());
        arg.putString("tacanF", pi.getTacan());
        PitanjeFrag frt = new PitanjeFrag();
        frt.setArguments(arg);
        getSupportFragmentManager().beginTransaction().replace(R.id.pitanjePlace,frt).commit();
    }
    public void createAlarm(String meessage, int hour, int minutes){
        Intent intent = new Intent(AlarmClock.ACTION_SET_ALARM).putExtra(AlarmClock.EXTRA_SKIP_UI,true).putExtra(AlarmClock.EXTRA_MESSAGE, meessage).putExtra(AlarmClock.EXTRA_HOUR, hour).putExtra(AlarmClock.EXTRA_MINUTES, minutes);
        if(intent.resolveActivity(getPackageManager()) != null){
            startActivity(intent);
        }
    }
    public int satiUDanu(){
        return Calendar.getInstance().get(java.util.Calendar.HOUR_OF_DAY);
    }
    public int minuteUDanu(){
        return Calendar.getInstance().get(Calendar.MINUTE);
    }
    public int izracunajX(int x, int y){
        double rez = (double)x/y;
        if((((double)x/y)/0.5)%2 == 1) rez = (double)x/y + 0.5;
        else rez = (double)x/y;
        return (int)rez;
    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}













