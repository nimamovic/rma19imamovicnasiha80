package ba.unsa.etf.rma.klase;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class MojProvider extends ContentProvider {
    private static final int ALLROWS = 1;
    private static final int ONEROW = 2;
    private static final UriMatcher uM;
    static{
        uM = new UriMatcher(UriMatcher.NO_MATCH);
        uM.addURI("rma.provider.kvizovi","elements",ALLROWS);
        uM.addURI("rma.provider.kvizovi","elements",ONEROW);
    }
    KvizoviDBOpenHelper kHelper;


    @Override
    public boolean onCreate() {
        kHelper = new KvizoviDBOpenHelper(getContext(),KvizoviDBOpenHelper.DATABASE_NAME,null, KvizoviDBOpenHelper.DATABASE_VERSION);
        return false;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        SQLiteDatabase baza;
        try{
            baza = kHelper.getWritableDatabase();
        }
        catch (SQLException e){
            baza = kHelper.getReadableDatabase();
        }
        String groupby = null;
        String having = null;
        SQLiteQueryBuilder squery = new SQLiteQueryBuilder();

        switch(uM.match(uri)){
            case ONEROW:
                String IDReda = uri.getPathSegments().get(1);
                squery.appendWhere(KvizoviDBOpenHelper.KVIZ_ID+"="+IDReda);
                default:break;
        }
        squery.setTables(KvizoviDBOpenHelper.DATABASE_TABLE​_KVIZ);
        Cursor cursor = squery.query(baza,projection,selection,selectionArgs,groupby,having,sortOrder);
        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        switch(uM.match(uri)){
            case ALLROWS:
                return "vnd.android.cursor.dir/vnd.rma.elemental";
            case ONEROW:
                return "vnd.android.cursor.dir/vnd.rma.elemental";
            default:
            {
                try {
                    throw new IllegalAccessException("Unsuported uri" + uri.toString());
                }
                catch (IllegalAccessException e){

                }
            }
        }
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        return null;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }
}
