package ba.unsa.etf.rma.klase;

public interface AsyncResponse {
    void processFinish();
}
