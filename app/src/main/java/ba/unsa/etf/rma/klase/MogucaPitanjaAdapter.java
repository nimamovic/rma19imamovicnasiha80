package ba.unsa.etf.rma.klase;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ba.unsa.etf.rma.R;

public class MogucaPitanjaAdapter extends ArrayAdapter<Pitanje> {
    private Context context;

    private List<Pitanje> pitanjeList = new ArrayList<Pitanje>();

    public MogucaPitanjaAdapter(@NonNull Context context, @Nullable ArrayList<Pitanje> list) {
        super(context, 0 , list);
        this.context = context;
        pitanjeList = list;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if(listItem == null)
            listItem = LayoutInflater.from(context).inflate(R.layout.lista,parent,false);

        Pitanje currentPitanje = pitanjeList.get(position);

        ImageView image = (ImageView)listItem.findViewById(R.id.slika);
        image.setImageResource(R.drawable.plus);

        TextView name = (TextView) listItem.findViewById(R.id.naziv);
        name.setText(currentPitanje.getNaziv());

        return listItem;
    }
}