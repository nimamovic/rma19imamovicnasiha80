package ba.unsa.etf.rma.fragmenti;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.aktivnosti.KvizoviAkt;
import ba.unsa.etf.rma.klase.Kategorija;
import ba.unsa.etf.rma.klase.KategorijaAdapter;
import ba.unsa.etf.rma.klase.Kviz;
import ba.unsa.etf.rma.klase.Pitanje;

public class ListaFrag extends Fragment {
    private ListView listaKategorija;

    private KategorijaAdapter adapterKat;

    private ArrayList<Kategorija> listaKat = new ArrayList<>();

    private Kategorija svi = new Kategorija("Svi","0");
    private ArrayList<Pitanje> pitanja = new ArrayList<Pitanje>();

    private ArrayList<Kviz> kviz = new ArrayList<Kviz>();

    private OnItemClick oic;

    public interface OnItemClick {
        void clickNaListu(ArrayList<Kviz> listaKvizova);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_lista, container, false);

        listaKategorija = (ListView) v.findViewById(R.id.listaKategorija);

        if (getArguments() != null) {

            listaKat = (ArrayList<Kategorija>) getArguments().getSerializable("KAT2");
        }
        adapterKat = new KategorijaAdapter(getActivity(), listaKat);
        listaKategorija.setAdapter(adapterKat);


        try{
            oic = (OnItemClick) getActivity();
        }
        catch (ClassCastException e){

        }

        listaKategorija.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Kategorija k = listaKat.get(position);
                    if (k.getNaziv().equals(svi.getNaziv())) {
                        kviz = new ArrayList<Kviz>();
                        for (Kviz q : KvizoviAkt.kvizovi) kviz.add(q);
                        oic.clickNaListu(kviz);
                    } else {
                        kviz = new ArrayList<Kviz>();
                        for (int i = 0; i < KvizoviAkt.kvizovi.size(); i++) {
                            if (KvizoviAkt.kvizovi.get(i).getKategorija().getNaziv().equals(k.getNaziv())) {
                                kviz.add(KvizoviAkt.kvizovi.get(i));
                            }
                        }
                        //kviz.add(new Kviz("Dodaj kviz", pitanja, svi));
                        oic.clickNaListu(kviz);
                    }
                }

                public void onNothingSelected(AdapterView<?> arg0) {
                    kviz = new ArrayList<Kviz>();
                    for (Kviz q : KvizoviAkt.kvizovi) kviz.add(q);
                    oic.clickNaListu(kviz);
                }
        });
        return v;

    }
}

