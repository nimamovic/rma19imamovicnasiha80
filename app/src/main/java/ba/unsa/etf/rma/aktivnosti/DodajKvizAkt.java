package ba.unsa.etf.rma.aktivnosti;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.common.collect.Lists;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.klase.AsyncResponse;
import ba.unsa.etf.rma.klase.DodanaPitanjaAdapter;
import ba.unsa.etf.rma.klase.Kategorija;
import ba.unsa.etf.rma.klase.KategorijaAdapter;
import ba.unsa.etf.rma.klase.Kviz;
import ba.unsa.etf.rma.klase.MogucaPitanjaAdapter;
import ba.unsa.etf.rma.klase.Pitanje;


public class DodajKvizAkt extends AppCompatActivity {
    private ListView lvDodanaPitanja;
    private ListView lvMogucaPitanja;
    private Spinner spKategorije;
    private EditText etNaziv;
    private Button btnDodajKviz;
    private Button btnImportKviz;

    private ArrayList<Pitanje> pitanjaDodana = new ArrayList<Pitanje>();
    private ArrayList<Pitanje> pitanjaMoguca = new ArrayList<Pitanje>();
    private ArrayList<Kategorija> katy = new ArrayList<Kategorija>();



    public static ArrayList<Pair<Pitanje, String>> aposlutnoSvaPitanja = new ArrayList<>();

    private Kviz kvizZaDodavanje = new Kviz();
    private String idKvizaZaEdit = "";
    public static ArrayList<String> idijeviPitanja = new ArrayList<>();
    public static String kategorijaZaDodavanjeKviza = new String();
    public static String nazivZaBazu2 = "";
    public static String ikonaZaBazu2 = "";

    private ArrayList<String> odgovori = new ArrayList<String>();

    private DodanaPitanjaAdapter adapterPitD;
    private MogucaPitanjaAdapter adapterPitM;
    private KategorijaAdapter adapterKat;

    private boolean jeLiEdit = false;
    private boolean daLiJeImport = false;

    private boolean daLiImaDodajPitanje = false;

    private boolean imaLiVecKviz = false;
    private boolean imaLiImeVec = false;
    private boolean imaLiVecKat = false;
    private boolean imaVecOdgovor = false;
    private int poy;
    private String naziv = "";

    private class KrerirajDokumentTaskZaKvizEdit extends AsyncTask<String, Void, Void> {
        AsyncResponse ar = null;

        public KrerirajDokumentTaskZaKvizEdit(AsyncResponse asyncResponse) {
            this.ar = asyncResponse;
        }

        @Override
        protected Void doInBackground(String... strings) {

            GoogleCredential credentials;
            try {

                InputStream is = getResources().openRawResource(R.raw.secret);
                credentials = GoogleCredential.fromStream(is).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                credentials.refreshToken();
                String TOKEN = credentials.getAccessToken();

                Log.d("TOKEN EVO GA", TOKEN);

                String url = "https://firestore.googleapis.com/v1/projects/projekat-20b5a/databases/(default)/documents/Kvizovi/" + idKvizaZaEdit + "?access_token=";
                URL urlObj = new URL(url + URLEncoder.encode(TOKEN, "UTF-8"));
                HttpURLConnection connection = (HttpURLConnection) urlObj.openConnection();
                connection.setDoOutput(true);
                connection.setRequestMethod("PATCH");
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setRequestProperty("Accept", "application/json");


                String dokument = "{ \"fields\": { \"naziv\": {\"stringValue\":\"" + kvizZaDodavanje.getNaziv() + "\"}, " +
                        "\"idKategorije\": {\"stringValue\" : \"" + kategorijaZaDodavanjeKviza + "\"}";

                dokument += " , \"pitanja\" : { \"arrayValue\" : { \"values\" : [";
                for (int j = 0; j < KvizoviAkt.svaPitanja.size(); j++) {
                    for (int i = 0; i < kvizZaDodavanje.getPitanja().size(); i++) {
                        if (kvizZaDodavanje.getPitanja().get(i).getNaziv().equals(KvizoviAkt.svaPitanja.get(j).first.getNaziv())) {
                            dokument += "{\"stringValue\" : \"" + KvizoviAkt.svaPitanja.get(j).second + "\"}";
                            if (j != KvizoviAkt.svaPitanja.size() - 1) dokument += ",";
                        }
                    }
                }
                //   DodajPitanjeAkt.pitanjaIzBaze.clear();
                dokument += " ] } } } }";

                for (int i = 0; i < KvizoviAkt.sviKvizovi.size(); i++) {
                    if (KvizoviAkt.sviKvizovi.get(i).second.equals(idKvizaZaEdit)) {
                        KvizoviAkt.sviKvizovi.get(i).first.setNaziv(kvizZaDodavanje.getNaziv());
                        KvizoviAkt.sviKvizovi.get(i).first.setPitanja(kvizZaDodavanje.getPitanja());
                        KvizoviAkt.sviKvizovi.get(i).first.setKategorija(kvizZaDodavanje.getKategorija());
                    }
                }
                ;

                try (OutputStream os = connection.getOutputStream()) {
                    byte[] input = dokument.getBytes("utf-8");
                    os.write(input, 0, input.length);
                }

                int code = connection.getResponseCode();
                InputStream odgovor = connection.getInputStream();
                try (BufferedReader br = new BufferedReader(
                        new InputStreamReader(odgovor, "utf-8"))
                ) {
                    StringBuilder response = new StringBuilder();
                    String rensponseLine = null;
                    while ((rensponseLine = br.readLine()) != null) {
                        response.append(rensponseLine.trim());
                    }
                    Log.d("ODGOVOR", response.toString());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

    @Override
    protected void onPostExecute(Void string) {
        ar.processFinish();
    }
}

    private class KrerirajDokumentTaskZaKviz extends AsyncTask<String,Void,Void> {
        @Override
        protected Void doInBackground(String... strings){

            GoogleCredential credentials;
            try {

                InputStream is = getResources().openRawResource(R.raw.secret);
                credentials = GoogleCredential.fromStream(is).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                credentials.refreshToken();
                String TOKEN = credentials.getAccessToken();

                Log.d("TOKEN EVO GA",TOKEN);

                String url = "https://firestore.googleapis.com/v1/projects/projekat-20b5a/databases/(default)/documents/Kvizovi?access_token=";
                URL urlObj = new URL (url + URLEncoder.encode(TOKEN, "UTF-8"));
                HttpURLConnection connection = (HttpURLConnection) urlObj.openConnection();
                connection.setDoOutput(true);
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setRequestProperty("Accept", "application/json");


                String dokument = "{ \"fields\": { \"naziv\": {\"stringValue\":\"" + kvizZaDodavanje.getNaziv() + "\"}, " +
                        "\"idKategorije\": {\"stringValue\" : \"" + kategorijaZaDodavanjeKviza + "\"}";

                dokument += " , \"pitanja\" : { \"arrayValue\" : { \"values\" : [";
                for(int j = 0; j < KvizoviAkt.svaPitanja.size(); j++){
                        for(int i = 0; i < kvizZaDodavanje.getPitanja().size(); i++) {
                            if(kvizZaDodavanje.getPitanja().get(i).getNaziv().equals(KvizoviAkt.svaPitanja.get(j).first.getNaziv()))
                            {
                                dokument += "{\"stringValue\" : \"" + KvizoviAkt.svaPitanja.get(j).second + "\"}";
                                if (j != KvizoviAkt.svaPitanja.size() - 1) dokument += ",";
                            }
                        }
                }
                dokument += " ] } } } }";



                try (OutputStream os = connection.getOutputStream()) {
                    byte[] input = dokument.getBytes("utf-8");
                    os.write(input, 0, input.length);
                }

                int code = connection.getResponseCode();
                InputStream odgovor = connection.getInputStream();
                try (BufferedReader br = new BufferedReader(
                        new InputStreamReader(odgovor, "utf-8"))
                ) {
                    StringBuilder response = new StringBuilder();
                    String rensponseLine = null;
                    while ((rensponseLine = br.readLine()) != null) {
                        response.append(rensponseLine.trim());
                    }
                    String katy = response.toString();
                    JSONObject jo = new JSONObject(katy);
                    JSONObject fields = jo.getJSONObject("fields");
                    String id = jo.getString("name");
                    int duzina = id.length();
                    String ime = "";
                    int brojac = 0;
                    for(int j = 0; j < id.length(); j++){
                        if(id.charAt(j) == '/' && brojac!=6){
                            brojac++;
                        }
                        else if(brojac == 6){
                            ime = id.substring(j, duzina);
                            break;
                        }
                    }

                    Pair<Kviz, String> pomP = new Pair<>(kvizZaDodavanje, ime);
                    KvizoviAkt.sviKvizovi.add(pomP);
                    Log.d("ODGOVOR", response.toString());
                }
            }
            catch (JSONException e){
                e.printStackTrace();
            }
            catch (IOException e){
                e.printStackTrace();
            }
            return null;
        }
    }
    public class KrerirajDokumentTaskZaMogucaPitanja extends AsyncTask<String,Void,Void> {
        AsyncResponse ar = null;

        public KrerirajDokumentTaskZaMogucaPitanja(AsyncResponse asyncResponse) {
            this.ar = asyncResponse;
        }

        @Override
        protected Void doInBackground(String... strings){

            GoogleCredential credentials;
            try {

                InputStream is = getResources().openRawResource(R.raw.secret);
                credentials = GoogleCredential.fromStream(is).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                credentials.refreshToken();
                String TOKEN = credentials.getAccessToken();

                Log.d("TOKEN EVO GA", TOKEN);


                String url2 = "https://firestore.googleapis.com/v1/projects/projekat-20b5a/databases/(default)/documents/Pitanja?access_token=";
                URL urlObj2 = new URL (url2 + URLEncoder.encode(TOKEN, "UTF-8"));
                HttpURLConnection connection = (HttpURLConnection) urlObj2.openConnection();
                connection.connect();
                InputStream odgovor = connection.getInputStream();
                StringBuilder response = null;
                try(BufferedReader br = new BufferedReader (
                        new InputStreamReader(odgovor, "utf-8"))
                ){
                    response = new StringBuilder();
                    String rensponseLine = null;
                    while((rensponseLine = br.readLine()) != null){
                        response.append(rensponseLine.trim());
                    }
                    Log.d("ODGOVOR ZA PITANJA", response.toString());
                }

                aposlutnoSvaPitanja.clear();
                String rezultat = response.toString();
                if(!rezultat.equals("{}")) {
                    JSONObject jo = new JSONObject(rezultat);
                    JSONArray documentsNiz = jo.getJSONArray("documents");
                    for (int p = 0; p < documentsNiz.length(); p++) {
                        JSONObject documentsO = documentsNiz.getJSONObject(p);
                        String name = documentsO.getString("name");
                        int duzina = name.length();
                        String ime = "";
                        int brojac = 0;
                        for (int j = 0; j < name.length(); j++) {
                            if (name.charAt(j) == '/' && brojac != 6) {
                                brojac++;
                            } else if (brojac == 6) {
                                ime = name.substring(j, duzina);
                                break;
                            }
                        }
                        JSONObject fields = documentsO.getJSONObject("fields");
                        JSONObject stringValue = fields.getJSONObject("naziv");
                        String naziv = stringValue.getString("stringValue");
                        JSONObject integerValue = fields.getJSONObject("indexTacnog");
                        int indexTacnog = integerValue.getInt("integerValue");
                        JSONObject odgovorIzbaze = fields.getJSONObject("odgovori");
                        JSONObject arrayValue = odgovorIzbaze.getJSONObject("arrayValue");
                        JSONArray values = arrayValue.getJSONArray("values");
                        ArrayList<String> listaOdgovora = new ArrayList<>();
                        for (int i = 0; i < values.length(); i++) {
                            JSONObject item = values.getJSONObject(i);
                            String odg = item.getString("stringValue");
                            listaOdgovora.add(odg);
                        }
                        String odgovorT = "";
                        for (int k = 0; k < listaOdgovora.size(); k++) {
                            if (k == indexTacnog) {
                                odgovorT = listaOdgovora.get(k);
                            }
                        }
                        Pitanje pomP = new Pitanje(naziv, odgovorT, listaOdgovora, odgovorT);
                        aposlutnoSvaPitanja.add(new Pair<Pitanje, String>(pomP,ime));

                    }
                }
            }
            catch (JSONException e){
                e.printStackTrace();
            }
            catch (IOException e){
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void string) {
            ar.processFinish();
        }
    }

    public class KrerirajDokumentTaskZaKategorijeZaImport extends AsyncTask<String,Void,Void> {

        @Override
        protected Void doInBackground(String... strings){

            GoogleCredential credentials;
            try {

                InputStream is = getResources().openRawResource(R.raw.secret);
                credentials = GoogleCredential.fromStream(is).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                credentials.refreshToken();
                String TOKEN = credentials.getAccessToken();

                Log.d("TOKEN EVO GA",TOKEN);

                String url = "https://firestore.googleapis.com/v1/projects/projekat-20b5a/databases/(default)/documents/Kategorije?access_token=";
                URL urlObj = new URL (url + URLEncoder.encode(TOKEN, "UTF-8"));
                HttpURLConnection connection = (HttpURLConnection) urlObj.openConnection();
                connection.setDoOutput(true);
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setRequestProperty("Accept", "application/json");


                String dokument = "{ \"fields\": { \"naziv\": {\"stringValue\":\"" + nazivZaBazu2 +"\"}, \"idIkonice\": {\"integerValue\":\"" + ikonaZaBazu2 +"\"}}}";

                try (OutputStream os = connection.getOutputStream())
                {

                    byte[] input = dokument.getBytes("utf-8");
                    os.write(input, 0, input.length);
                }

                int code = connection.getResponseCode();
                InputStream odgovor = connection.getInputStream();
                try(BufferedReader br = new BufferedReader (
                        new InputStreamReader(odgovor, "utf-8"))
                ){
                    StringBuilder response = new StringBuilder();
                    String rensponseLine = null;
                    while((rensponseLine = br.readLine()) != null){
                        response.append(rensponseLine.trim());
                    }
                    String katy = response.toString();
                    JSONObject jo = new JSONObject(katy);
                    JSONObject fields = jo.getJSONObject("fields");
                    JSONObject jsonNaziva = fields.getJSONObject("naziv");
                    String naziv = jsonNaziva.getString("stringValue");
                    JSONObject jsonIkone = fields.getJSONObject("idIkonice");
                    int idIkonice = jsonIkone.getInt(("integerValue"));
                    String ik = String.valueOf(idIkonice);
                    String id = jo.getString("name");
                    int duzina = id.length();
                    String ime = "";
                    int brojac = 0;
                    for(int j = 0; j < id.length(); j++){
                        if(id.charAt(j) == '/' && brojac!=6){
                            brojac++;
                        }
                        else if(brojac == 6){
                            ime = id.substring(j, duzina);
                            break;
                        }
                    }

                    Kategorija pomK = new Kategorija(naziv, ik);
                    Pair<Kategorija, String> pomP = new Pair<>(pomK, ime);
                    DodajKategorijuAkt.kategorijeIzBaze.add(pomP);
                    KvizoviAkt.sveKategorije.add(pomP);

                    Log.d("ODGOVOR", response.toString());
                }
            }
            catch(JSONException e){
                e.printStackTrace();
            }
            catch (IOException e){
                e.printStackTrace();
            }
            return null;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dodaj_kviz_akt);

        lvDodanaPitanja = (ListView) findViewById(R.id.lvDodanaPitanja);
        lvMogucaPitanja = (ListView) findViewById(R.id.lvMogucaPitanja);
        spKategorije = (Spinner) findViewById(R.id.spKategorije);
        etNaziv = (EditText) findViewById(R.id.etNaziv);
        btnDodajKviz = (Button) findViewById(R.id.btnDodajKviz);
        btnImportKviz = (Button) findViewById(R.id.btnImportKviz);



        if (getIntent().getExtras() != null) {
            naziv = getIntent().getStringExtra("naziv");
            ArrayList<Pitanje> p = (ArrayList<Pitanje>) getIntent().getSerializableExtra("listapitanja");
            for (Pitanje q : p) pitanjaDodana.add(q);
            katy.add((Kategorija) getIntent().getSerializableExtra("kategorija"));
            jeLiEdit = true;
            poy = KvizoviAkt.pozicija;

        }
        etNaziv.setText(naziv);

        //if (getIntent().getExtras() == null)
        if(!pitanjaDodana.contains(new Pitanje("Dodaj pitanje"))) pitanjaDodana.add(new Pitanje("Dodaj pitanje"));

        adapterPitD = new DodanaPitanjaAdapter(this, pitanjaDodana);
        lvDodanaPitanja.setAdapter(adapterPitD);
        /*new KrerirajDokumentTaskZaMogucaPitanja(
                new AsyncResponse() {
                    @Override
                    public void processFinish() {
                        pitanjaMoguca.clear();
                        boolean postoji = false;
                        for(int k = 0 ; k < aposlutnoSvaPitanja.size(); k++){
                            for(int i = 0 ; i < KvizoviAkt.pitanjaUKvizovima.size(); i++){
                                if(aposlutnoSvaPitanja.get(k).first.getNaziv().equals(KvizoviAkt.pitanjaUKvizovima.get(i).first.getNaziv())) postoji = true;
                            }
                            if(!postoji) pitanjaMoguca.add(aposlutnoSvaPitanja.get(k).first);
                            postoji = false;
                        }
                        adapterPitM = new MogucaPitanjaAdapter(DodajKvizAkt.this, pitanjaMoguca);
                        lvMogucaPitanja.setAdapter(adapterPitM);
                    }
                }).execute();*/
        if(isNetworkAvailable()){
            new KrerirajDokumentTaskZaMogucaPitanja(
                    new AsyncResponse() {
                        @Override
                        public void processFinish() {
                            pitanjaMoguca.clear();
                            boolean postoji = false;
                            for(int k = 0 ; k < aposlutnoSvaPitanja.size(); k++){
                                for(int i = 0; i < pitanjaDodana.size(); i++){
                                    if(aposlutnoSvaPitanja.get(k).first.getNaziv().equals(pitanjaDodana.get(i).getNaziv())) postoji = true;
                                }
                                if(!postoji) pitanjaMoguca.add(aposlutnoSvaPitanja.get(k).first);
                                postoji = false;
                            }
                            adapterPitM = new MogucaPitanjaAdapter(DodajKvizAkt.this, pitanjaMoguca);
                            lvMogucaPitanja.setAdapter(adapterPitM);
                            adapterPitM.notifyDataSetChanged();
                        }
                    }).execute();
        }
        else {
            boolean postoji = false;
            for(int k = 0; k < KvizoviAkt.svaPitanja.size(); k++){
                for(int i = 0; i < pitanjaDodana.size(); i++){
                    if(KvizoviAkt.svaPitanja.get(k).first.getNaziv().equals(pitanjaDodana.get(i).getNaziv())) postoji = true;
                }
                if(!postoji) pitanjaMoguca.add(KvizoviAkt.svaPitanja.get(k).first);
                postoji = false;
            }
            adapterPitM = new MogucaPitanjaAdapter(DodajKvizAkt.this, pitanjaMoguca);
            lvMogucaPitanja.setAdapter(adapterPitM);
            adapterPitM.notifyDataSetChanged();
        }


       lvMogucaPitanja.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(isNetworkAvailable()) {
                    int index = pitanjaDodana.size() - 1;
                    Pitanje p = pitanjaMoguca.get(position);
                    pitanjaDodana.add(index, p);
                    pitanjaMoguca.remove(position);
                    adapterPitD.notifyDataSetChanged();
                    adapterPitM.notifyDataSetChanged();
                }
            }
        });

        lvDodanaPitanja.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(isNetworkAvailable()) {
                    if (position == pitanjaDodana.size() - 1) {
                        Intent myIntent = new Intent(DodajKvizAkt.this, DodajPitanjeAkt.class);
                        DodajKvizAkt.this.startActivityForResult(myIntent, 0);
                    } else {
                        pitanjaMoguca.add(pitanjaDodana.get(position));
                        pitanjaDodana.remove(position);
                        adapterPitM.notifyDataSetChanged();
                        adapterPitD.notifyDataSetChanged();
                    }
                }

            }
        });

        for (Kategorija k : KvizoviAkt.kategorije) {
            if (!katy.contains(k)) katy.add(k);
        }
        katy.add(new Kategorija("Dodaj kategoriju", "1"));
        adapterKat = new KategorijaAdapter(this, katy);
        spKategorije.setAdapter(adapterKat);
        adapterKat.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spKategorije.setFocusableInTouchMode(true);


        spKategorije.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View view, int position, long arg3) {
                if (position == katy.size() - 1) {
                    if(isNetworkAvailable()) {
                        Intent myIntent = new Intent(DodajKvizAkt.this, DodajKategorijuAkt.class);
                        DodajKvizAkt.this.startActivityForResult(myIntent, 3);
                    }
                    //startActivity(myIntent);
                } else {
                    spKategorije.setSelection(position);

                }
            }

            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });


        btnDodajKviz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isNetworkAvailable()) {
                    imaLiVecKviz = false;
                    for (Kviz q : KvizoviAkt.kvizovi)
                        if (q.getNaziv().equals(etNaziv.getText().toString())) {
                            if (jeLiEdit) {
                                if (q.getNaziv().equals(naziv)) imaLiVecKviz = false;
                            } else imaLiVecKviz = true;
                        }
                    if (etNaziv.getText().toString().length() == 0) {
                        etNaziv.setBackgroundColor(Color.RED);
                    } else if (imaLiVecKviz) {
                        etNaziv.setBackgroundColor(Color.RED);

                    } else if (jeLiEdit == true) {
                        KvizoviAkt.pozicija = poy;
                        etNaziv.setBackgroundColor(Color.WHITE);
                        Kategorija l = (Kategorija) spKategorije.getSelectedItem();
                        Kviz kviz = new Kviz(etNaziv.getText().toString(), pitanjaDodana, l);
                        idijeviPitanja.clear();
                        for (int r = 0; r < KvizoviAkt.svaPitanja.size(); r++) {
                            for (int o = 0; o < pitanjaDodana.size(); o++)
                                if (KvizoviAkt.svaPitanja.get(r).first.getNaziv().equals(pitanjaDodana.get(o)))
                                    idijeviPitanja.add(KvizoviAkt.svaPitanja.get(r).second);
                        }
                        kvizZaDodavanje = kviz;
                        for (int i = 0; i < KvizoviAkt.sviKvizovi.size(); i++) {
                            if (naziv.equals(KvizoviAkt.sviKvizovi.get(i).first.getNaziv()))
                                idKvizaZaEdit = KvizoviAkt.sviKvizovi.get(i).second;
                        }
                        System.out.println("id kvizaaaaaa" + idKvizaZaEdit);
                        boolean postavljenaKategorija = false;
                        for (int i = 0; i < DodajKategorijuAkt.kategorijeIzBaze.size(); i++) {
                            if (DodajKategorijuAkt.kategorijeIzBaze.get(i).first.getNaziv().equals(l.getNaziv())) {
                                kategorijaZaDodavanjeKviza = DodajKategorijuAkt.kategorijeIzBaze.get(i).second;
                                postavljenaKategorija = true;
                                break;
                            }
                        }
                        if (!postavljenaKategorija) {
                            if (!daLiJeImport) {
                                for (int i = 0; i < KvizoviAkt.sveKategorije.size(); i++) {
                                    if (KvizoviAkt.sveKategorije.get(i).first.getNaziv().equals(l.getNaziv())) {
                                        kategorijaZaDodavanjeKviza = KvizoviAkt.sveKategorije.get(i).second;
                                        postavljenaKategorija = true;
                                        break;
                                    }
                                }
                            }
                        }
                        if (!KvizoviAkt.kategorije.contains(l)) KvizoviAkt.kategorije.add(l);
                        new KrerirajDokumentTaskZaKvizEdit(new AsyncResponse() {
                            @Override
                            public void processFinish() {
                                adapterPitD.notifyDataSetChanged();
                                adapterPitM.notifyDataSetChanged();
                            }
                        }).execute();
                        Intent returnIntent = getIntent();
                        returnIntent.putExtra("KVIZ", kviz.getNaziv());
                        returnIntent.putExtra("PIT", kviz.getPitanja());
                        returnIntent.putExtra("KAT", kviz.getKategorija());
                        setResult(RESULT_OK, returnIntent);
                        finish();
                    } else if (jeLiEdit != true) {
                        etNaziv.setBackgroundColor(Color.WHITE);
                        Kategorija l = (Kategorija) spKategorije.getSelectedItem();
                        if (!KvizoviAkt.kategorije.contains(l)) KvizoviAkt.kategorije.add(l);
                        Kviz kviz = new Kviz(etNaziv.getText().toString(), pitanjaDodana, l);
                        kvizZaDodavanje = kviz;
                        boolean postavljenaKategorija = false;
                        for (int i = 0; i < DodajKategorijuAkt.kategorijeIzBaze.size(); i++) {
                            if (DodajKategorijuAkt.kategorijeIzBaze.get(i).first.getNaziv().equals(l.getNaziv())) {
                                kategorijaZaDodavanjeKviza = DodajKategorijuAkt.kategorijeIzBaze.get(i).second;
                                postavljenaKategorija = true;
                                break;
                            }
                        }
                        if (!postavljenaKategorija) {
                            if (!daLiJeImport) {
                                for (int i = 0; i < KvizoviAkt.sveKategorije.size(); i++) {
                                    if (KvizoviAkt.sveKategorije.get(i).first.getNaziv().equals(l.getNaziv())) {
                                        kategorijaZaDodavanjeKviza = KvizoviAkt.sveKategorije.get(i).second;
                                        postavljenaKategorija = true;
                                        break;
                                    }
                                }
                            }
                        }
                        new KrerirajDokumentTaskZaKviz().execute();
                        Intent returnIntent = getIntent();
                        returnIntent.putExtra("KVIZ", kviz.getNaziv());
                        returnIntent.putExtra("PIT", kviz.getPitanja());
                        returnIntent.putExtra("KAT", kviz.getKategorija());
                        setResult(RESULT_OK, returnIntent);
                        finish();
                    }
                }
            }
        });


        btnImportKviz.setOnClickListener(new View.OnClickListener()
                {
        @Override
        public void onClick(View v) {
            if (isNetworkAvailable()) {
                daLiJeImport = true;
                Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("text/*");
                startActivityForResult(intent, 2);
            }
        }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {

        if (requestCode == 3) {
            if (resultCode == RESULT_OK) {
                KvizoviAkt.kategorije.add(KvizoviAkt.kategorije.size(), new Kategorija(intent.getStringExtra("KATEGORIJA"), intent.getStringExtra("id")));
                katy.add(0, new Kategorija(intent.getStringExtra("KATEGORIJA"), intent.getStringExtra("id")));
                adapterKat = new KategorijaAdapter(this, katy);
                spKategorije.setAdapter(adapterKat);
            }

        }
        else if (requestCode == 0){
            if (resultCode == RESULT_OK) {
                new KrerirajDokumentTaskZaMogucaPitanja(new AsyncResponse() {
                    @Override
                    public void processFinish() {
                        pitanjaMoguca.clear();
                        boolean postoji = false;
                        for(int k = 0 ; k < aposlutnoSvaPitanja.size(); k++){
                            for(int i = 0 ; i < KvizoviAkt.pitanjaUKvizovima.size(); i++){
                                if(aposlutnoSvaPitanja.get(k).first.getNaziv().equals(KvizoviAkt.pitanjaUKvizovima.get(i).first.getNaziv())) postoji = true;
                            }
                            if(!postoji) pitanjaMoguca.add(aposlutnoSvaPitanja.get(k).first);
                            postoji = false;
                        }
                        adapterPitM = new MogucaPitanjaAdapter(DodajKvizAkt.this, pitanjaMoguca);
                        lvMogucaPitanja.setAdapter(adapterPitM);
                        adapterPitM.notifyDataSetChanged();
                        adapterPitD.notifyDataSetChanged();
                    }
                }).execute();
                String naziv = intent.getStringExtra("PITANJE");
                for(Pitanje q : pitanjaDodana) if(q.getNaziv().equals(naziv)) daLiImaDodajPitanje = true;
                Pitanje p =  new Pitanje(intent.getStringExtra("PITANJE"), intent.getStringExtra("PITANJE2"), (ArrayList<String>) intent.getSerializableExtra("ODG"), intent.getStringExtra("TACAN"));
                odgovori = (ArrayList<String>) intent.getSerializableExtra("ODG");
                p.setOdgovori(odgovori);
                if(!daLiImaDodajPitanje) pitanjaDodana.add(pitanjaDodana.size() - 1,p);
                adapterPitD.notifyDataSetChanged();

            }
        }
        else if(requestCode == 2){
            ArrayList<Pitanje> pitanjaImport = new ArrayList<Pitanje>();
            Kviz kvizImport = new Kviz();
            boolean greska = false;
            if(resultCode == RESULT_OK) {

                int brojPitanja;
                int sljedeci = 0;
                Uri uri = null;
                if (intent != null) {
                    uri = intent.getData();
                    ArrayList<String> dat = null;
                    try {
                        dat = readTextFromUri(uri);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    for (int i = 0; i < dat.size(); i++) {
                        int pom = 0;
                        if (i == 0) {
                            sljedeci = 0;
                            for (int j = 0; j < dat.get(i).length(); j++) {
                                if (dat.get(i).charAt(j) == ',' && sljedeci == 0) {
                                    String ime = dat.get(i).substring(pom, j);
                                    pom = j + 1;
                                    sljedeci = 1;
                                    imaLiImeVec = false;
                                    for (int k = 0; k < KvizoviAkt.kvizovi.size(); k++) {
                                        if (KvizoviAkt.kvizovi.get(k).getNaziv().equals(ime)) {
                                            imaLiImeVec = true;
                                            // break;
                                        }
                                    }
                                    if (imaLiImeVec) {
                                        greska = true;
                                        AlertDialog.Builder builder1 = new AlertDialog.Builder(DodajKvizAkt.this);
                                        builder1.setMessage("Kviz kojeg importujete već postoji!");
                                        builder1.setCancelable(true);

                                        builder1.setPositiveButton(
                                                "OK",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        dialog.cancel();
                                                    }
                                                });



                                        AlertDialog alert11 = builder1.create();
                                        alert11.show();
                                    } else {
                                        kvizImport.setNaziv(ime);
                                        //etNaziv.setText(kvizImport.getNaziv());
                                        sljedeci = 1;
                                        j++;
                                    }
                                }
                                if (!imaLiImeVec){
                                    if (dat.get(i).charAt(j) == ',' && sljedeci == 1) {
                                        String k = dat.get(i).substring(pom, j);
                                        //etNaziv.setText(k);
                                        kvizImport.setKategorija(new Kategorija(k, "0"));
                                        imaLiVecKat = false;
                                        for (int m = 0; m < KvizoviAkt.kategorije.size(); m++) {
                                            if (KvizoviAkt.kategorije.get(m).getNaziv().equals(k)) {

                                             //   kvizImport.setKategorija(KvizoviAkt.kategorije.get(m));
                                                imaLiVecKat = true;
                                            }
                                        }
                                        kvizImport.setKategorija(new Kategorija(k, "0"));
                                        pom = j + 1;
                                        j++;
                                        sljedeci = 2;
                                    }
                                if (sljedeci == 2) {
                                    try {
                                        brojPitanja = Integer.parseInt(dat.get(i).substring(pom, ++j));
                                        if (brojPitanja != dat.size() - 1) {
                                            greska = true;
                                            AlertDialog.Builder builder1 = new AlertDialog.Builder(DodajKvizAkt.this);
                                            builder1.setMessage("Kviz kojeg imporujete ima neispravan broj pitanja!");
                                            builder1.setCancelable(true);

                                            builder1.setPositiveButton(
                                                    "OK",
                                                    new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog, int id) {
                                                            dialog.cancel();
                                                        }
                                                    });


                                            AlertDialog alert11 = builder1.create();
                                            alert11.show();
                                        }
                                    } catch (Exception e) {
                                        greska = true;
                                        AlertDialog.Builder builder1 = new AlertDialog.Builder(DodajKvizAkt.this);
                                        builder1.setMessage("Kviz kojeg imporujete ima neispravan broj pitanja!");
                                        builder1.setCancelable(true);

                                        builder1.setPositiveButton(
                                                "OK",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        dialog.cancel();
                                                    }
                                                });


                                        AlertDialog alert11 = builder1.create();
                                        alert11.show();
                                    }
                                    sljedeci = 3;
                                }
                            }

                            }
                        }
                        else{
                            if(!imaLiImeVec && !greska) {
                                imaVecOdgovor = false;
                                sljedeci = 0;
                                ArrayList<String> odgovroi = new ArrayList<>();
                                pom = 0;
                                int brojOdgovora = 0;
                                int tacanOdgovor = -1;
                                Pitanje p = new Pitanje();
                                for (int j = 0; j < dat.get(i).length(); j++) {
                                    if (dat.get(i).charAt(j) == ',' && sljedeci == 0) {
                                        p.setNaziv(dat.get(i).substring(pom, j));
                                        for(int m = 0; m < pitanjaImport.size(); m++){
                                            if(pitanjaImport.get(m).getNaziv().equals(p.getNaziv()) && !greska){
                                                greska = true;
                                                AlertDialog.Builder builder1 = new AlertDialog.Builder(DodajKvizAkt.this);
                                                builder1.setMessage("Kviz nije ispravan postoje dva pitanja sa istim nazivom!");
                                                builder1.setCancelable(true);

                                                builder1.setPositiveButton(
                                                        "OK",
                                                        new DialogInterface.OnClickListener() {
                                                            public void onClick(DialogInterface dialog, int id) {
                                                                dialog.cancel();
                                                            }
                                                        });


                                                AlertDialog alert11 = builder1.create();
                                                alert11.show();
                                            }
                                        }
                                        pom = j + 1;
                                       // j++;
                                        sljedeci = 1;
                                    }
                                    else if (dat.get(i).charAt(j) == ',' && sljedeci == 1) {
                                        try {
                                            brojOdgovora = Integer.parseInt(dat.get(i).substring(pom, j));
                                            pom = j + 1;
                                            sljedeci = 2;
                                          //  j++;
                                        }
                                        catch (Exception e){

                                        }

                                    }
                                    else if (dat.get(i).charAt(j) == ',' && sljedeci == 2 && j!=pom) {
                                        try {
                                            if(!greska) {
                                                tacanOdgovor = Integer.parseInt(dat.get(i).substring(pom, j));
                                                pom = j + 1;
                                                sljedeci = 3;
                                                if ((tacanOdgovor < 0 || tacanOdgovor > brojOdgovora) && !greska) {
                                                    greska = true;
                                                    AlertDialog.Builder builder1 = new AlertDialog.Builder(DodajKvizAkt.this);
                                                    builder1.setMessage("Kviz kojeg importujete ima neispravan index tačnog odgovora!");
                                                    builder1.setCancelable(true);

                                                    builder1.setPositiveButton(
                                                            "OK",
                                                            new DialogInterface.OnClickListener() {
                                                                public void onClick(DialogInterface dialog, int id) {
                                                                    dialog.cancel();
                                                                }
                                                            });


                                                    AlertDialog alert11 = builder1.create();
                                                    alert11.show();
                                                }
                                            }
                                        }
                                        catch (Exception e){
                                            greska = true;
                                            AlertDialog.Builder builder1 = new AlertDialog.Builder(DodajKvizAkt.this);
                                            builder1.setMessage("Kviz kojeg importujete ima neispravan index tačnog odgovora!");
                                            builder1.setCancelable(true);

                                            builder1.setPositiveButton(
                                                    "OK",
                                                    new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog, int id) {
                                                            dialog.cancel();
                                                        }
                                                    });


                                            AlertDialog alert11 = builder1.create();
                                            alert11.show();
                                        }
                                    }
                                    else if (dat.get(i).charAt(j) == ',' && sljedeci == 2 && j==pom) {
                                        greska = true;
                                        AlertDialog.Builder builder1 = new AlertDialog.Builder(DodajKvizAkt.this);
                                        builder1.setMessage("Kviz kojeg importujete ima neispravan index tačnog odgovora!");
                                        builder1.setCancelable(true);

                                        builder1.setPositiveButton(
                                                "OK",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        dialog.cancel();
                                                    }
                                                });


                                        AlertDialog alert11 = builder1.create();
                                        alert11.show();
                                        break;
                                    }
                                    else if ((dat.get(i).charAt(j) == ',' || j == dat.get(i).length() -1) && sljedeci == 3) {
                                        if(j == dat.get(i).length() - 1) {
                                            if (odgovroi.contains(dat.get(i).substring(pom, j+1)))
                                                imaVecOdgovor = true;
                                            odgovroi.add(dat.get(i).substring(pom, j+1));
                                        }
                                        else if( j < dat.get(i).length() - 1){
                                            if (odgovroi.contains(dat.get(i).substring(pom, j)))
                                             imaVecOdgovor = true;
                                            odgovroi.add(dat.get(i).substring(pom, j));
                                            pom = j + 1;
                                            sljedeci = 3;
                                        }
                                    }


                                }
                                if(imaVecOdgovor && !greska){
                                    greska = true;
                                    AlertDialog.Builder builder1 = new AlertDialog.Builder(DodajKvizAkt.this);
                                    builder1.setMessage("Kviz kojeg importujete nije ispravan postoji ponavljanje odgovora!");
                                    builder1.setCancelable(true);

                                    builder1.setPositiveButton(
                                            "OK",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.cancel();
                                                }
                                            });


                                    AlertDialog alert11 = builder1.create();
                                    alert11.show();
                                }
                                if(odgovroi.size() != brojOdgovora && !imaVecOdgovor && !greska){
                                    greska = true;
                                    AlertDialog.Builder builder1 = new AlertDialog.Builder(DodajKvizAkt.this);
                                    builder1.setMessage("Kviz kojeg importujete ima neispravan broj odgovora!");
                                    builder1.setCancelable(true);

                                    builder1.setPositiveButton(
                                            "OK",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.cancel();
                                                }
                                            });


                                    AlertDialog alert11 = builder1.create();
                                    alert11.show();
                                }
                                if(!greska) {
                                    p.setOdgovori(odgovroi);
                                    if( tacanOdgovor != -1) p.setTacan(odgovroi.get(tacanOdgovor));
                                    pitanjaImport.add(p);
                                }
                            }
                        }
                    }
                    if(!greska){
                        kvizImport.setPitanja(pitanjaImport);
                    }


                }
                if(!greska){
                    etNaziv.setText(kvizImport.getNaziv());
                    if(!imaLiVecKat) {
                        katy.add(0, kvizImport.getKategorija());
                        nazivZaBazu2 = kvizImport.getKategorija().getNaziv();
                        ikonaZaBazu2 = kvizImport.getKategorija().getId();
                        new KrerirajDokumentTaskZaKategorijeZaImport().execute();
                    }
                    else{
                        for(int i= 0; i < KvizoviAkt.sveKategorije.size(); i++){
                            if(kvizImport.getKategorija().getNaziv().equals(KvizoviAkt.sveKategorije.get(i).first.getNaziv())){
                                kategorijaZaDodavanjeKviza = KvizoviAkt.sveKategorije.get(i).second;
                                daLiJeImport = true;
                            }
                        }
                    }
                    adapterKat = new KategorijaAdapter(this, katy);
                    spKategorije.setAdapter(adapterKat);
                    pitanjaDodana.removeAll(pitanjaDodana);
                    pitanjaDodana.add(new Pitanje("Dodaj pitanje"));
                    pitanjaDodana.addAll(pitanjaDodana.size() - 1, kvizImport.getPitanja());
                    adapterPitD.notifyDataSetChanged();
                }
            }

        }
        if (resultCode == RESULT_CANCELED) {
            //Write your code if there's no result
        }



    }
    private ArrayList<String> readTextFromUri(Uri uri) throws IOException {
        ArrayList<String> lines = new ArrayList<>();
        InputStream inputStream = getContentResolver().openInputStream(uri);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            lines.add(line);
        }
        lines.remove(lines.size() - 1);
        return lines;
    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}
