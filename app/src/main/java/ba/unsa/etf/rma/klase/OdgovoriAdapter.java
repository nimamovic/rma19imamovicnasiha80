package ba.unsa.etf.rma.klase;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ba.unsa.etf.rma.R;

public class OdgovoriAdapter extends ArrayAdapter<Pair<String,Boolean>> {
    private Context context;

    private List<Pair<String,Boolean>> odgovoriList = new ArrayList<Pair<String,Boolean>>();

    public OdgovoriAdapter(@NonNull Context context, @Nullable ArrayList<Pair<String,Boolean>> list) {
        super(context, 0 , list);
        this.context = context;
        odgovoriList = list;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if(listItem == null)
            listItem = LayoutInflater.from(context).inflate(R.layout.lista,parent,false);

        Pair<String,Boolean> currentOdgovor = odgovoriList.get(position);

        ImageView image = (ImageView)listItem.findViewById(R.id.slika);
        image.setImageResource(R.drawable.krug);

        TextView text = (TextView) listItem.findViewById(R.id.naziv);

        if(currentOdgovor.second == true) {
            text.setBackgroundColor(Color.GREEN);
            text.setText(currentOdgovor.first);
        }
        else{
            text.setBackgroundColor(0);
            text.setText(currentOdgovor.first);
        }

        return listItem;
    }
}